---
title: "Site audit"
date: 2021-08-27T12:14:00+02:00
weight: 20
---

## Typical usages for Site audit

* have a global overview the accessibility level of an entire website (say
50'000 pages)
* identify pages or parts of a site with excessive accessibility issues

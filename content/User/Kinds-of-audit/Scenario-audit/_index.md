---
title: "Scenario audit"
date: 2021-06-10T16:14:00+02:00
weight: 30
---

<!---
*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***

     This page is linked from within the Asqatasun app
     in /web-app/asqatasun-web-app/src/main/resources/i18n/scenario-management-I18N(..)

     Do not rename, move or delete please :)

*** WARNING *** WARNING *** WARNING *** WARNING *** WARNING *** WARNING ***
-->


## Typical usages for Scenario audit

* automatically measure a form that is split in several pages
* assess the page given by a search result
* audit a **web application**
* measure **different states of a very same page**

## Pre-requisites

Client side:

* [Firefox](https://www.mozilla.org/firefox/new/) version 91 ESR
* Extension [Selenium IDE](https://addons.mozilla.org/fr/firefox/addon/selenium-ide/)

Server side:

* Asqatasun v5.0.0-rc.1 at least

## How to create a scenario

* Record your scenario with Selenium IDE
* Verify it actually works withing Selenium IDE
* Upload it to Asqatasun

**Remarks:**

* in Selenium IDE, you will create a *testsuite*, compound of one *test*. This test is the actual scenario,
and it can have several steps. If you ever create more than one *test* (from a Selenium IDE point of view),
only the first one will be executed by Asqatasun.

## Understanding Asqatasun scenarios

A scenario is a set of Selenium commands, either recorded thanks to Selenium-IDE addon, either coded by a developer.

Once uploaded, a scenario is played on Asqatasun server. Technically, this means a headless Firefox (managed
by Asqatasun) executes the desired scenario. This Firefox has an empty default profile with English as language.

Resources:

* Selenium API Reference: [commands](https://www.selenium.dev/selenium-ide/docs/en/api/commands)
* Selenium API Reference: [arguments](https://www.selenium.dev/selenium-ide/docs/en/api/arguments)

## Replay locally before uploading

You'll save **a lot** of time doing so, as you'll have no clue on what's going wrong if the scenario audit fails.

When replaying locally, you may encounter a CSP ([content security policy](https://content-security-policy.com/)) error:

![](Selenium-IDE_error_Call_to_eval_blocked_by_CSP.png)

An old solution consisted in disabling CSP in the browser. This is
[no longer possible since Firefox 99](https://bugzilla.mozilla.org/show_bug.cgi?id=1754301). Another way to do so can be
done by using a web extension.

Here is how to replay locally your scenario. First install the
[Firefox extension ModHeader](https://addons.mozilla.org/fr/firefox/addon/modheader-firefox/). Once installed,
open the panel:

![](Modheader_00.png)

Click on the `+` icon and add a `Response header`: as name type `Content-Security-Policy` and as value
type `script-src: 'unsafe-eval'`.

Click on more time on the `+` icon and add a `Filter`. Let say your scenario is for `https://www.example.org/`,
for the `URL pattern` type `https:\/\/www.example.org/`.

![](Modheader_01_CSP_disabled_for_www.example.org.png)

You may also leverage [Firefox profiles](https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles)
to have a separated environment for Selenium-IDE. To do so create a new profile, have Selenium-IDE extension installed,
and simply replay the scenario to verify it actually works (you might need ModHeader to replay locally your scenario).

## Scenario best practices

### Default language is English

When creating a scenario, keep in mind the Firefox that will run it, is pristine ; no session in any website is created,
no cookie is stored yet, no authentication has been made yet.

So if you create a scenario on an internationalized web application, and want to test it (let say) in French, you'll
have to trigger the relevant actions to switch the web application to French. Those actions must be part of the
scenario, then you can record the business tasks you want to audit.

On the other side, the Asqatasun administrator could install a localized version of Firefox ESR (instead of
the default English version).

## Examples of scenario

You can find various scenario examples in our dedicated repository: [Asqatasun scenario examples](https://gitlab.com/asqatasun/scenario-examples)

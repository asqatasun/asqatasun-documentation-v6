---
title: "Kinds of audit"
date: 2021-06-10T16:14:00+02:00
weight: 30
---

Asqatasun offers different kinds of audit. Choose the suitable kind depending on your need.

* [Page audit](Page-audit/)
* [Site audit](Site-audit/)
* [Scenario audit](Scenario-audit/)
* [Offline file audit](Offline-file-audit)

<!-- markdownlint-disable MD033 -->
<h2 id="audit-comparison">Comparison of audits</h2>
<!-- markdownlint-enable MD033 -->

| Audit                        |       Page       |     Scenario     | Site |   Offline file   |
|:-----------------------------|:----------------:|:----------------:|:----:|:----------------:|
| DOM / Generated HTML support |       YES        |       YES        | YES  |       YES        |
| Deals with CSS               |       YES        |       YES        | YES  |       YES        |
| Deals with colors            |       YES        |       YES        | YES  |       YES        |
| Comply with `robots.txt`     | *Not Applicable* | *Not Applicable* | YES  | *Not Applicable* |

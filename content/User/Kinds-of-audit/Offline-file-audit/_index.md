---
title: "Offline file audit"
date: 2021-06-10T16:14:00+02:00
weight: 40
---

## Typical usages for Offline file audit

* A developer is creating a template which is still on its machine (not yet on the
webserver), and wants to have an **early statement of accessibility**
* User wants to audit an intranet page (not connected to the internet). Just save the
page and send it as offline file to Asqatasun

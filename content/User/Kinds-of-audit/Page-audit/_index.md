---
title: "Page audit"
date: 2021-06-10T16:14:00+02:00
weight: 10
---

## Typical usages

*The* typical audit you run on a page to have insights about its level of accessibility.

## Quick steps (for the impatient)

1. For a given project, click the *Audit pages* link
1. Click the *Launch the audit* button
1. Have the results

## Detailed steps

### Pre-requisite

* You need a user account with at least 1 project configured with **Page audit** option.
* If this is not the case, you must contact the administrator of your Asqatasun instance.
* If you have an administrator account, you can add or update projects directly in the
  [back-office](../../../Administrator).

### Step 1

Let say we have a project for the [BBC website](https://www.bbc.com/), click the
*Audit pages* link.

![](screenshot_20150307_ASQATASUN_PAGE_audit_BBC_00.png)

### Step 2a

Type in the URL of the page to be audited.

![](screenshot_20150307_ASQATASUN_PAGE_audit_BBC_10.png)

**Notes:**

* The URL must be on the domain (here under <www.bbc.com>, like <www.bbc.com/mypage.html>)
* Sub-domains are considered different sites. E.g sports.bbc.com can't be audited
with the <www.bbc.com> project (you have to use a dedicated project for sports.bbc.com)

### Step 2b

Eventually adjust the [Options](#page-audit-options).

### Step 2c

Click the *Launch the audit* button.

### Step 3

Have the result.

![](screenshot_20150307_ASQATASUN_PAGE_audit_BBC_30_result.png)

## Page audit options

![](screenshot_20150307_ASQATASUN_PAGE_audit_BBC_20_options.png)

### Level

For a given referential, select a level (priority), for instance A, AA or AAA.

### HTML markers

HTML Markers are used to increase automation. Please see the [dedicated page for
HTML markers](../../HTML-markers)

### Colour contrast

This disables all the tests on colour contrasts. Check this box if the website
offers a CSS to have valid contrasts (like on [https://careers.societegenerale.com/](https://careers.societegenerale.com/)).

---
title: "Asqatasun usage"
weight: 10
---

## Use webapp

Browse <http://localhost:8080/> and connect with credentials:

- login: `admin@asqatasun.org`
- password: `myAsqaPassword`

## Use server with Swagger

You may leverage Swagger user interface by browsing <http://localhost:8080/> and connect with credentials:

- login: `admin@asqatasun.org`
- password: `myAsqaPassword`

## Use server with cURL

### Request

```shell
ASQA_USER="admin%40asqatasun.org"                                                                                                                    
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8080"
API_URL="${API_PREFIX_URL}/api/v0/audit/page/run"

PROJECT_ID="1"
REFERENTIAL="RGAA_4_0"
LEVEL="AA"
URL_TO_AUDIT="https://www.wikidata.org/"

curl -X POST \
     "${API_URL}"                                               \
     -H  "accept: */*"                                          \
     -H  "Content-Type: application/json"                       \
     -d "{                                                      \
            \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                           \"referential\": \"${REFERENTIAL}\", \
                           \"level\": \"${LEVEL}\",             \
                           \"contractId\": ${PROJECT_ID},       \
                           \"tags\": []                         \
         }"
```


### Response

API returns the ID of the audit that has just been launched.

```shell
7 # Audit ID
```

### Display the result of an audit via API

Display the result of an audit via API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"

AUDIT_ID="7"
API_URL="${API_PREFIX_URL}/api/v0/audit/${AUDIT_ID}"
curl -X GET "${API_URL}" -H  "accept: */*"
```

### Audit details (JSON)

API returns a JSON content which contains information of requested audit (status, results, ...).

```json
{
  "referential": "RGAA_4_0",
  "subject": {
    "grade": "E",
    "type": "PAGE",
    "repartitionBySolutionType": [
      {
        "type": "PASSED",
        "number": 8
      },
      {
        "number": 3,
        "type": "FAILED"
      },
      {
        "number": 29,
        "type": "NEED_MORE_INFO"
      },
      {
        "number": 71,
        "type": "NOT_APPLICABLE"
      },
      {
        "type": "NOT_TESTED",
        "number": 146
      }
    ],
    "id": 1,
    "url": "https://www.wikidata.org/wiki/Wikidata:Main_Page",
    "mark": 72.73,
    "nbOfPages": 1
  },
  "id": 1,
  "status": "COMPLETED",
  "date": "2020-11-07T14:08:36.000+0000",
  "referentialLevel": "LEVEL_2",
  "tags": []
}
```

## Client for Asqatasun Server

You may also leverage the [PHP API client](https://gitlab.com/asqatasun/api-client-php).

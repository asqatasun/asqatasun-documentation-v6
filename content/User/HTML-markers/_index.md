---
title: "HTML Markers"
date: 2021-06-10T16:14:00+02:00
weight: 50
---

Leverage Accessibility automation with HTML markers.

HTML markers are used to give Asqatasun more information. For instance, if you know your
dev team typically codes data table with a `class="data-table"`, just give this
information to Asqatasun. This way more tests can be achieved.

Markers are available for:

* data tables
* presentation tables
* decorative images
* meaningful images

The value passed as a marker will be checked against the following attributes:

* `class`
* `id`
* `role`

---
title: "Exporting data"
date: 2021-06-10T16:14:00+02:00
weight: 80
---

Asqatasun can export page-audit into those formats:

* CSV (Comma Separated Value) See [Details about CSV export structure](CSV-Export)
* PDF (Portable Document Format)
* ODS (LibreOffice)

**Note**: To grab data, you may also use [the API](../../Developer/API/)

---
title: "5 types of result"
date: 2021-06-10T16:14:00+02:00
weight: 20
---

Asqatasun presents five types of results.

![Asqatasun 5 types of results screenshot](screenshot_20150307_ASQATASUN_5_types_of_result.png)

The definitive results (given by the accessibility methodology itself):

* Passed
* Failed
* Not Applicable

The non-definitive results :

* **Pre-qualified**: any test where Asqatasun can't state reliably a definitive result
but can actually bring any useful information, that will lead in **time-savings** for
the user
* **Not Tested**: all the other tests.


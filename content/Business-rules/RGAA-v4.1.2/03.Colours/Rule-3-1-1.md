---
title: "Rule 3.1.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[3.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#3.1)

### Test

[3.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#3.1.1)

### Description

> Pour chaque mot ou ensemble de mots dont la mise en couleur est porteuse d’information, l’
> [information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#information-donnee-par-la-couleur)
> ne doit pas être donnée uniquement par la couleur. Cette règle est-elle respectée ?

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 3.1.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule030101/)
- [Unit test file for rule 3.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule030101Test.java)
- [Class file for rule 3.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule030101.java)



---
title: "Rule 3.2.5"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[3.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#3.2)

### Test

[3.2.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#3.2.5)

### Description

> Dans le
> [mécanisme qui permet d’afficher un rapport de contraste](https://accessibilite.numerique.gouv.fr/methode/glossaire/#mecanisme-qui-permet-d-afficher-un-rapport-de-contraste-conforme)
> conforme, le rapport de contraste entre le texte et la couleur d’arrière-plan est-il suffisamment élevé ?


#### Cas particuliers (3.2)

> Dans ces situations, les critères sont non applicables pour ces éléments
>
> * Le texte fait partie d’un logo ou d’un nom de marque d’un organisme ou d’une société
> * Le texte ou l’image de texte est purement décoratif
> * Le texte fait partie d’une image véhiculant une information mais le texte lui-même n’apporte aucune information
> essentielle
> * Le texte ou l’image de texte fait partie d’un élément d’interface sur lequel aucune action n’est possible (par
> exemple un bouton avec l’attribut `disabled`).

### Level

AA


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 3.2.5](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule030205/)
* [Unit test file for rule 3.2.5](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule030205Test.java)
* [Class file for rule 3.2.5](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule030205.java)



---
title: "Rule 7.3.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[7.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.3)

### Test

[7.3.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.3.2)

### Description

> Un
> [script](https://accessibilite.numerique.gouv.fr/methode/glossaire/#script)
> ne doit pas supprimer le focus d’un élément qui le reçoit. Cette règle est-elle respectée (hors cas particuliers) ?


#### Cas particuliers (7.3)

> Il existe une gestion de cas particuliers lorsque la fonctionnalité dépend de l’utilisation d’un gestionnaire
> d’événement sans équivalent universel par exemple, une application de dessin à main levée ne pourra pas être rendue
> contrôlable au clavier. Dans ces situations, le critère est non applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 7.3.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule070302/)
- [Unit test file for rule 7.3.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070302Test.java)
- [Class file for rule 7.3.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070302.java)



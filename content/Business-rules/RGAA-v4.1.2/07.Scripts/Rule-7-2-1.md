---
title: "Rule 7.2.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[7.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.2)

### Test

[7.2.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.2.1)

### Description

> Chaque
> [script](https://accessibilite.numerique.gouv.fr/methode/glossaire/#script)
> débutant par la balise `<script>` et ayant une
> [alternative](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-a-script)
> vérifie-t-il une de ces conditions?
>
> * L’
> [alternative](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-a-script)
> entre `<noscript>` et `</noscript>` permet d’accéder à des contenus et des fonctionnalités similaires
> * La page affichée, lorsque JavaScript est désactivé, permet d’accéder à des contenus et des fonctionnalités
> similaires
> * La page alternative permet d’accéder à des contenus et des fonctionnalités similaires
> * Le langage de script côté serveur permet d’accéder à des contenus et des fonctionnalités similaires
> * L’alternative présente dans la page permet d’accéder à des contenus et des fonctionnalités similaires.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 7.2.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule070201/)
* [Unit test file for rule 7.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070201Test.java)
* [Class file for rule 7.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070201.java)



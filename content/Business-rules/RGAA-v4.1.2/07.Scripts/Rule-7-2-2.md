---
title: "Rule 7.2.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[7.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.2)

### Test

[7.2.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.2.2)

### Description

> Chaque élément non textuel mis à jour par un
> [script](https://accessibilite.numerique.gouv.fr/methode/glossaire/#script)
> (dans la page, ou dans un
> [cadre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#cadre)
> ) et ayant une
> [alternative](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-a-script)
> vérifie-t-il ces conditions?
>
> * L’alternative de l’élément non textuel est mise à jour
> * L’alternative mise à jour est pertinente.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 7.2.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule070202/)
* [Unit test file for rule 7.2.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070202Test.java)
* [Class file for rule 7.2.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070202.java)



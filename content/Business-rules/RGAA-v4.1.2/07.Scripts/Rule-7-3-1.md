---
title: "Rule 7.3.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[7.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.3)

### Test

[7.3.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.3.1)

### Description

> Chaque élément possédant un gestionnaire d’événement contrôlé par un script vérifie-t-il une de ces conditions (hors
> cas particuliers)?
>
> * L’élément est
> [accessible par le clavier et tout dispositif de pointage](https://accessibilite.numerique.gouv.fr/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage)
> * Un élément
> [accessible par le clavier et tout dispositif de pointage](https://accessibilite.numerique.gouv.fr/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage)
> permettant de réaliser la même action est présent dans la page.


#### Cas particuliers (7.3)

> Il existe une gestion de cas particuliers lorsque la fonctionnalité dépend de l’utilisation d’un gestionnaire
> d’événement sans équivalent universel par exemple, une application de dessin à main levée ne pourra pas être rendue
> contrôlable au clavier. Dans ces situations, le critère est non applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 7.3.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule070301/)
* [Unit test file for rule 7.3.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070301Test.java)
* [Class file for rule 7.3.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070301.java)



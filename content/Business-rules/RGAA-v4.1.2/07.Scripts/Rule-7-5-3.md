---
title: "Rule 7.5.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[7.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.5)

### Test

[7.5.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#7.5.3)

### Description

> Chaque
> [message de statut](https://accessibilite.numerique.gouv.fr/methode/glossaire/#message-de-statut)
> qui indique la progression d’un processus utilise-t-il l’un des attributs WAI-ARIA `role="log"`, `role="progressbar"`
> ou `role="status"` ?


#### Notes techniques (7.5)

> Les rôles WAI-ARIA `log`, `status` et `alert` ont implicitement une valeur d’attribut WAI-ARIA `aria-live` et
> `aria-atomic`. On pourra donc considérer (conformément à la spécification WAI-ARIA 1.1) que
>
> * Un attribut WAI-ARIA `aria-live="polite"` associé à un message de statut peut valoir pour un rôle WAI-ARIA `log`
> * Un attribut WAI-ARIA `aria-live="polite"` et un attribut WAI-ARIA `aria-atomic="true"` associés à un message de
> statut peuvent valoir pour un rôle WAI-ARIA `status`
> * Un attribut WAI-ARIA `aria-live="assertive"` et un attribut WAI-ARIA `aria-atomic="true"` associés à un message de
> statut peuvent valoir pour un rôle WAI-ARIA `alert`.
> C’est sous réserve que la nature du message de statut satisfasse bien à la correspondance implicitement établie. Dans
> le cas d’un message de statut indiquant la progression d’un processus et matérialisé graphiquement par une barre de
> progression, un rôle WAI-ARIA `progressbar` explicite est nécessaire.

### Level

AA


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 7.5.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule070503/)
* [Unit test file for rule 7.5.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070503Test.java)
* [Class file for rule 7.5.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule070503.java)



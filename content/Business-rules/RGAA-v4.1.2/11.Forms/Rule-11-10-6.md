---
title: "Rule 11.10.6"
date: 2024-08-11T11:54:33+02:00
weight: 10
---

## Summary

This test consists in detecting `<form>` tags on the page.

The control that checks error messages has to be done manually.

## Business description

### Criterion

[11.10](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#11.10)

### Test

[11.10.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#11.10.6)

### Description

> Les messages d’erreurs fournissant une instruction ou une indication du type de données et/ou de format obligatoire
> des champs vérifient-ils une de ces conditions?
>
> * Le message d’erreur fournissant une instruction ou une indication du type de données et/ou de format obligatoires
> est visible et identifie le champ concerné
> * Le champ dispose de l’attribut `aria-invalid="true"`.


#### Notes techniques (11.10)

> Dans un long formulaire dont la majorité des champs sont obligatoires, on pourrait constater que ce sont les quelques
> champs restés facultatifs qui sont explicitement signalés comme tels. Dans ce cas, il faudrait s’assurer que
>
> * Un message précise visuellement en haut de formulaire que “tous les champs sont obligatoires sauf ceux indiqués
> comme étant facultatifs”
> * Une mention “facultatif” est présente visuellement dans le libellé des champs facultatifs ou dans la légende d’un
> groupe de champs facultatifs
> * Un attribut `required` ou `aria-required="true"` reste associé à chaque champ qui n’est pas concerné par ce
> caractère facultatif.


#### Cas particuliers (11.10)

> Le test 11.10.1 et le test 11.10.2 seront considérés comme non applicables lorsque le formulaire comporte un seul
> [champ de formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#champ-de-saisie-de-formulaire)
> ou qu’il indique les champs optionnels de manière
>
> * Visible
> * Dans la balise `<label>` ou dans la
> [légende](https://accessibilite.numerique.gouv.fr/methode/glossaire/#legende)
> associée au champ.
> Dans le cas où l’ensemble des champs d’un formulaire sont obligatoires, les tests 11.10.1 et 11.10.2 restent
> applicables.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

All the `<form>` tags.

css selector :

```css
form
```

### Process

#### Test1

The selection handles the process.

For each occurence of the **Set1** raise a MessageA

##### MessageA: Manual check on element

* code: ManualCheckOnElements
* status: Pre-Qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

The page has no `<form>` tag (**Set1** is empty)

#### Pre-qualified

In all other cases


## Notes

We detect the elements of the scope of the test to determine whether the test is applicable.

## Files

* [TestCases files for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule111006/)
* [Unit test file for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule111006Test.java)
* [Class file for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule111006.java)



---
title: "Rule 11.4.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[11.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#11.4)

### Test

[11.4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#11.4.1)

### Description

> Chaque
> [étiquette de champ](https://accessibilite.numerique.gouv.fr/methode/glossaire/#etiquette-de-champ-de-formulaire)
> et son
> [champ](https://accessibilite.numerique.gouv.fr/methode/glossaire/#champ-de-saisie-de-formulaire)
> associé sont-ils
> [accolés](https://accessibilite.numerique.gouv.fr/methode/glossaire/#accoles-etiquette-et-champ-accoles)
> ?


#### Cas particuliers (11.4)

> Les tests 11.4.2 et 11.4.3 seront considérés comme non applicables
>
> * Dans le cas où l’
> [étiquette](https://accessibilite.numerique.gouv.fr/methode/glossaire/#etiquette-de-champ-de-formulaire)
> mélange une portion de texte qui se lit de droite à gauche avec une portion de texte qui se lit de gauche à droite
> * Dans le cas où un formulaire contient des labels de plusieurs langues qui se liraient de droite à gauche et
> inversement. Par exemple, un formulaire de commande en arabe qui propose une liste de cases à cocher de produit en
> langue française ou mixant des produits en langue arabe ou en langue française
> * Dans le cas où les champs de type `radio` ou `checkbox` et les balises ayant un attribut WAI-ARIA
> `role="checkbox"`, `role="radio"` ou `role="switch"` ne sont pas visuellement présentés sous forme de bouton radio ou
> de case à cocher
> * Dans le cas où les champs seraient utilisés dans un contexte où il pourrait être légitime, du point de vue de
> l’expérience utilisateur, de placer les étiquettes de manière différente à celle requise dans les tests 11.4.2 et
> 11.4.3.

### Level

AA


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 11.4.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule110401/)
* [Unit test file for rule 11.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule110401Test.java)
* [Class file for rule 11.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule110401.java)



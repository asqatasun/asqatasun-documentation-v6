---
title: "Theme 11: Forms"
date: 2020-11-15T18:18:18+02:00
weight: 15
---

## Criterion 11.1

> Chaque
> [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
> a-t-il une
> [étiquette](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquette-de-champ-de-formulaire)
> ?

* [Rule 11.1.1]({{< relref "./Rule-11-1-1" >}})
* [Rule 11.1.2]({{< relref "./Rule-11-1-2" >}})
* [Rule 11.1.3]({{< relref "./Rule-11-1-3" >}})

## Criterion 11.2

> Chaque
> [étiquette](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquette-de-champ-de-formulaire)
> associée à un
> [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
> est-elle pertinente (hors cas particuliers) ?

* [Rule 11.2.1]({{< relref "./Rule-11-2-1" >}})
* [Rule 11.2.2]({{< relref "./Rule-11-2-2" >}})
* [Rule 11.2.3]({{< relref "./Rule-11-2-3" >}})
* [Rule 11.2.4]({{< relref "./Rule-11-2-4" >}})
* [Rule 11.2.5]({{< relref "./Rule-11-2-5" >}})
* [Rule 11.2.6]({{< relref "./Rule-11-2-6" >}})

## Criterion 11.3

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , chaque
> [étiquette](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquette-de-champ-de-formulaire)
> associée à un
> [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
> ayant la même fonction et répété plusieurs fois dans une même page ou dans un
> [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages)
> est-elle
> [cohérente](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquettes-coherentes) ?

* [Rule 11.3.1]({{< relref "./Rule-11-3-1" >}})
* [Rule 11.3.2]({{< relref "./Rule-11-3-2" >}})

## Criterion 11.4

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , chaque
> [étiquette de champ](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquette-de-champ-de-formulaire)
> et son champ associé sont-ils
> [accolés](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accoles-etiquette-et-champ-accoles)
> (hors cas particuliers) ?

* [Rule 11.4.1]({{< relref "./Rule-11-4-1" >}})
* [Rule 11.4.2]({{< relref "./Rule-11-4-2" >}})
* [Rule 11.4.3]({{< relref "./Rule-11-4-3" >}})

## Criterion 11.5

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , les  
> [champs de même nature](https://accessibilite.numerique.gouv.fr/methode/glossaire/#champs-de-meme-nature)
> sont-ils regroupés, si nécessaire ?

* [Rule 11.5.1]({{< relref "./Rule-11-5-1" >}})

## Criterion 11.6

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , chaque regroupement de
> [champs de même nature](https://accessibilite.numerique.gouv.fr/methode/glossaire/#champs-de-meme-nature)
> a-t-il une
> [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) ?

* [Rule 11.6.1]({{< relref "./Rule-11-6-1" >}})

## Criterion 11.7

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , chaque
> [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) associée à un
> regroupement de
> [champs de même nature](https://accessibilite.numerique.gouv.fr/methode/glossaire/#champs-de-meme-nature)
> est-elle pertinente ?

* [Rule 11.7.1]({{< relref "./Rule-11-7-1" >}})

## Criterion 11.8

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> ,
> [les items de même nature d’une liste de choix](https://accessibilite.numerique.gouv.fr/methode/glossaire/#items-de-meme-nature-d-une-liste-de-choix)
> sont-ils regroupées de manière pertinente ?

* [Rule 11.8.1]({{< relref "./Rule-11-8-1" >}})
* [Rule 11.8.2]({{< relref "./Rule-11-8-2" >}})
* [Rule 11.8.3]({{< relref "./Rule-11-8-3" >}})

## Criterion 11.9

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , l’intitulé de chaque
> [bouton](https://accessibilite.numerique.gouv.fr/methode/glossaire/#bouton-formulaire)
> est-il pertinent (hors cas particuliers) ?

* [Rule 11.9.1]({{< relref "./Rule-11-9-1" >}})
* [Rule 11.9.2]({{< relref "./Rule-11-9-2" >}})

## Criterion 11.10

> Dans chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , le
> [contrôle de saisie](https://accessibilite.numerique.gouv.fr/methode/glossaire/#controle-de-saisie-formulaire)
> est-il utilisé de manière pertinente (hors cas particuliers) ?

* [Rule 11.10.1]({{< relref "./Rule-11-10-1" >}})
* [Rule 11.10.2]({{< relref "./Rule-11-10-2" >}})
* [Rule 11.10.3]({{< relref "./Rule-11-10-3" >}})
* [Rule 11.10.4]({{< relref "./Rule-11-10-4" >}})
* [Rule 11.10.5]({{< relref "./Rule-11-10-5" >}})
* [Rule 11.10.6]({{< relref "./Rule-11-10-6" >}})
* [Rule 11.10.7]({{< relref "./Rule-11-10-7" >}})

## Criterion 11.11

> Dans chaque
> [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> , le
> [contrôle de saisie](https://accessibilite.numerique.gouv.fr/methode/glossaire/#controle-de-saisie-formulaire)
> est-il accompagné, si nécessaire, de suggestions facilitant la
> correction des erreurs de saisie ?

* [Rule 11.11.1]({{< relref "./Rule-11-11-1" >}})
* [Rule 11.11.2]({{< relref "./Rule-11-11-2" >}})

## Criterion 11.12

> Pour chaque [formulaire](https://accessibilite.numerique.gouv.fr/methode/glossaire/#formulaire)
> qui modifie ou supprime des données, ou qui transmet des réponses à un test ou à un examen, ou
> dont la validation a des conséquences financières ou juridiques, les données saisies peuvent-elles être modifiées,
> mises à jour ou récupérées par l’utilisateur ?

* [Rule 11.12.1]({{< relref "./Rule-11-12-1" >}})
* [Rule 11.12.2]({{< relref "./Rule-11-12-2" >}})

## Criterion 11.13

> La finalité d’un champ de saisie peut-elle être déduite pour faciliter le remplissage automatique des champs avec les
> données de l’utilisateur ?

* [Rule 11.13.1]({{< relref "./Rule-11-13-1" >}})


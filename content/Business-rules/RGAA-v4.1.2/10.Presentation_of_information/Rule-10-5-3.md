---
title: "Rule 10.5.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[10.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#10.5)

### Test

[10.5.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#10.5.3)

### Description

> Dans chaque page web, chaque utilisation d’une image pour créer une couleur de fond d’un élément susceptible de
> contenir du texte, via CSS (`background`, `background-image`), est-elle accompagnée d’une déclaration de couleur de
> fond (`background`, `background-color`), au moins, héritée d’un parent ?

### Level

AA


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 10.5.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule100503/)
- [Unit test file for rule 10.5.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule100503Test.java)
- [Class file for rule 10.5.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule100503.java)



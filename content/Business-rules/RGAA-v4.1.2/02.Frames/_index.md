---
title: "Theme 2: Frames"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 2.1

> Chaque
> [cadre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#cadre)
> a-t-il un
> [titre de cadre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#titre-de-cadre) ?

* [Rule 2.1.1]({{< relref "./Rule-2-1-1" >}})

## Criterion 2.2

> Pour chaque
> [cadre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#cadre)
> ayant un
> [titre de cadre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#titre-de-cadre)
> , ce titre de cadre est-il pertinent ?

* [Rule 2.2.1]({{< relref "./Rule-2-2-1" >}})


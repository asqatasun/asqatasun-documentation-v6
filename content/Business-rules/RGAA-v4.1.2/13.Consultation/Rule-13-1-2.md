---
title: "Rule 13.1.2"
date: 2024-08-11T11:54:33+02:00
weight: 2
---

## Summary

This test consists in checking whether a refresh action declared via the `<meta>` tag is immediate.

## Business description

### Criterion

[13.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.1)

### Test

[13.1.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.1.2)

### Description

> Pour chaque page web, chaque procédé de
> [redirection](https://accessibilite.numerique.gouv.fr/methode/glossaire/#redirection)
> effectué via une balise `<meta>` est-il immédiat (hors cas particuliers) ?


#### Cas particuliers (13.1)

> Il existe une gestion de cas particuliers lorsque la limite de temps est essentielle, notamment lorsqu’elle ne
> pourrait pas être supprimée sans changer fondamentalement le contenu ou les fonctionnalités liées au contenu.
> Dans ces situations, le critère est non applicable. Par exemple, le rafraîchissement d’un flux RSS dans une page
> n’est pas une limite de temps essentielle le critère est applicable. En revanche, une redirection automatique qui
> amène vers la nouvelle version d’une page à partir d’une URL obsolète est essentielle le critère est non applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

Decidable

## Algorithm

### Selection

#### Set1

All the `<meta>` tags within a head with a `http-equiv` attribute equals to "refresh" and a `content` attribute that
contains the "URL" or "url" occurence.

### Process

#### Test1

We check whether the size of **Set1** is equal to 1.

#### Test2

If **Test1** is TRUE, we extract the delay value of the refresh regarding the definition of meta tag (@see Notes)

Then we check whether the delay value is superior to 0. If true raise a messageA

##### MessageA : Not Immediate Redirection via Meta

- code: NotImmediateRedirectionViaMeta
- status: Failed
- parameter: tag name
- present in source: yes

### Analysis

#### Not Applicable

- The **Set1** is empty
- Test1 returns FALSE

#### Failed

- Test2 returns TRUE

#### Passed

- Test2 returns FALSE

## Notes

We assume that the meta tag has to be defined has follows:

```html
<meta http-equiv="refresh" content="delay;URL='URL'">
```


## Files

- [TestCases files for rule 13.1.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule130102/)
- [Unit test file for rule 13.1.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130102Test.java)
- [Class file for rule 13.1.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130102.java)


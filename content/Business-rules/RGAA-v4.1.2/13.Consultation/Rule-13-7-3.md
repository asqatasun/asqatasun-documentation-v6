---
title: "Rule 13.7.3"
date: 2024-08-11T11:54:33+02:00
weight: 2
---

## Summary

No-check rule

## Business description

### Criterion

[13.7](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.7)

### Test

[13.7.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.7.3)

### Description

> Dans chaque page web, chaque mise en forme CSS qui provoque
> [un changement brusque de luminosité ou un effet de flash](https://accessibilite.numerique.gouv.fr/methode/glossaire/#changement-brusque-de-luminosite-ou-effet-de-flash)
> vérifie-t-il une de ces conditions?
>
> * La fréquence de l’effet est inférieure à 3 par seconde
> * La surface totale cumulée des effets est inférieure ou égale à 21824 pixels.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 13.7.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule130703/)
* [Unit test file for rule 13.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130703Test.java)
* [Class file for rule 13.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130703.java)



---
title: "Rule 13.1.4"
date: 2024-08-11T11:54:33+02:00
weight: 2
---

## Summary

No-check rule

## Business description

### Criterion

[13.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.1)

### Test

[13.1.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#13.1.4)

### Description

> Pour chaque page web, chaque procédé limitant le temps d’une session vérifie-t-il une de ces conditions (hors cas
> particuliers)?
>
> * L’utilisateur peut supprimer la limite de temps
> * L’utilisateur peut augmenter la limite de temps
> * La limite de temps avant la fin de la session est de vingt heures au moins.


#### Cas particuliers (13.1)

> Il existe une gestion de cas particuliers lorsque la limite de temps est essentielle, notamment lorsqu’elle ne
> pourrait pas être supprimée sans changer fondamentalement le contenu ou les fonctionnalités liées au contenu.
> Dans ces situations, le critère est non applicable. Par exemple, le rafraîchissement d’un flux RSS dans une page
> n’est pas une limite de temps essentielle le critère est applicable. En revanche, une redirection automatique qui
> amène vers la nouvelle version d’une page à partir d’une URL obsolète est essentielle le critère est non applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 13.1.4](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule130104/)
* [Unit test file for rule 13.1.4](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130104Test.java)
* [Class file for rule 13.1.4](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule130104.java)



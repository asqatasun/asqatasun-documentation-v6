---
title: "Rule 5.4.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[5.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.4)

### Test

[5.4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.4.1)

### Description

> Pour chaque
> [tableau de données ayant un titre](https://accessibilite.numerique.gouv.fr/methode/glossaire/#tableau-de-donnees-ayant-un-titre)
> , le titre est-il correctement associé au tableau de données ?

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 5.4.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule050401/)
- [Unit test file for rule 5.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050401Test.java)
- [Class file for rule 5.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050401.java)



---
title: "Rule 5.6.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[5.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.6)

### Test

[5.6.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.6.2)

### Description

> Pour chaque
> [tableau de données](https://accessibilite.numerique.gouv.fr/methode/glossaire/#tableau-de-donnees)
> , chaque
> [en-tête de ligne](https://accessibilite.numerique.gouv.fr/methode/glossaire/#en-tete-de-colonne-ou-de-ligne)
> s’appliquant à la totalité de la ligne vérifie-t-il une de ces conditions?
>
> * L’
> [en-tête de lignes](https://accessibilite.numerique.gouv.fr/methode/glossaire/#en-tete-de-colonne-ou-de-ligne)
> est structuré au moyen d’une balise `<th>`
> * L’
> [en-tête de lignes](https://accessibilite.numerique.gouv.fr/methode/glossaire/#en-tete-de-colonne-ou-de-ligne)
> est structuré au moyen d’une balise pourvue d’un attribut WAI-ARIA `role="rowheader"`.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 5.6.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule050602/)
* [Unit test file for rule 5.6.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050602Test.java)
* [Class file for rule 5.6.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050602.java)



---
title: "Rule 5.7.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[5.7](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.7)

### Test

[5.7.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.7.3)

### Description

> Pour chaque contenu de balise `<th>` ne s’appliquant pas à la totalité de la ligne ou de la colonne, la balise `<th>`
> vérifie-t-elle ces conditions?
>
> * La balise `<th>` ne possède pas d’attribut `scope`
> * La balise `<th>` ne possède pas d’attribut WAI-ARIA `role="rowheader"` ou `role="columnheader"`
> * La balise `<th>` possède un attribut `id` unique.


#### Notes techniques (5.7)

> Si l’attribut `headers` est implémenté sur une cellule déjà reliée à un en-tête (de ligne ou de colonne) avec
> l’attribut `scope` (avec la valeur `col` ou `row`), c’est l’en-tête ou les en-têtes référencés par l’attribut
> `headers` qui seront restitués aux technologies d’assistance. Les en-têtes reliés avec l’attribut `scope` seront
> ignorés.


#### Cas particuliers (5.7)

> Dans le cas de tableaux de données ayant des en-têtes sur une seule ligne ou une seule colonne, les en-têtes peuvent
> être structurés à l’aide de balise `<th>` sans attribut `scope`.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 5.7.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule050703/)
* [Unit test file for rule 5.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050703Test.java)
* [Class file for rule 5.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050703.java)



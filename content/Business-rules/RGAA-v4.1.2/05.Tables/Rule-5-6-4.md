---
title: "Rule 5.6.4"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[5.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.6)

### Test

[5.6.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.6.4)

### Description

> Pour chaque
> [tableau de données](https://accessibilite.numerique.gouv.fr/methode/glossaire/#tableau-de-donnees)
> , chaque cellule associée à plusieurs en-têtes est-elle structurée au moyen d’une balise `<td>` ou `<th>` ?

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 5.6.4](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule050604/)
* [Unit test file for rule 5.6.4](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050604Test.java)
* [Class file for rule 5.6.4](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050604.java)



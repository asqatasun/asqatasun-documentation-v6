---
title: "Rule 5.8.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[5.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.8)

### Test

[5.8.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#5.8.1)

### Description

> Chaque
> [tableau de mise en forme](https://accessibilite.numerique.gouv.fr/methode/glossaire/#tableau-de-mise-en-forme)
> (balise `<table>`) vérifie-t-il ces conditions?
>
> * Le tableau de mise en forme (balise `<table>`) n’a pas d’attribut `summary` (sinon vide) et ne contient pas de
> balises `<caption>`, `<th>`, `<thead>`, `<tfoot>` ou de balises ayant un attribut WAI-ARIA `role="rowheader"`,
> `role="columnheader"`
> * Les cellules du tableau de mise en forme (balises `<td>`) ne possèdent pas d’attributs `scope`, `headers` et
> `axis`.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 5.8.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule050801/)
* [Unit test file for rule 5.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050801Test.java)
* [Class file for rule 5.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule050801.java)



---
title: "Rule 1.6.9"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[1.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.6)

### Test

[1.6.9](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.6.9)

### Description

> Pour chaque image (balise `<img>`, `<input>` avec l’attribut `type="image"`, `<area>`, `<object>`, `<embed>`,
> `<svg>`, `<canvas>`, ou possédant un attribut WAI-ARIA `role="img"`)
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> , qui est accompagnée d’une
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> et qui utilise un attribut WAI-ARIA `aria-describedby`, l’attribut WAI-ARIA `aria-describedby` associe-t-il la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> ?


#### Notes techniques (1.6)

> Dans le cas du SVG, le manque de support de l’élément `<title>` et `<desc>` par les technologies d’assistance crée
> une difficulté dans le cas de l’implémentation de l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> de l’image et de sa
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> . Dans ce cas, il est recommandé d’utiliser l’attribut WAI-ARIA `aria-label` pour implémenter à la fois l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> courte et la référence à la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente ou l’attribut WAI-ARIA `aria-labelledby` pour associer les passages de texte faisant office d’alternative
> courte et de
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> .
> L’utilisation de l’attribut WAI-ARIA aria-describedby n’est pas recommandée pour lier une image (`<img>`, `<object>`,
> `<embed>`, `<canvas>`) à sa
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> , par manque de support des technologies d’assistance. Néanmoins, lorsqu’il est utilisé, l’attribut devra
> nécessairement faire référence à l’`id` de la zone contenant la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> .
> La
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente peut être implémentée via une balise `<figcaption>`, dans ce cas le critère 1.9 doit être vérifié
> (utilisation de `<figure>` et des attributs WAI-ARIA `role="figure"` et `aria-label`, notamment).
> L'attribut `longdesc` qui constitue une des conditions du test 1.6.1 (et dont la pertinence est vérifiée avec le test
> 1.7.1) est désormais considéré comme obsolète par la spécification HTML en cours. La vérification de cet attribut ne
> sera donc requise que pour les versions de la spécification HTML antérieure à HTML 5.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 1.6.9](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010609/)
- [Unit test file for rule 1.6.9](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010609Test.java)
- [Class file for rule 1.6.9](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010609.java)



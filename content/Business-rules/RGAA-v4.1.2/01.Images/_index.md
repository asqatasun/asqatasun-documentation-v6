---
title: "Theme 1: Images"
date: 2020-11-12T17:14:53+02:00
weight: 9
---

## Criterion 1.1

> Chaque
> [image porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information)
> a-t-elle une
> [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image)
> ?

* [Rule 1.1.1]({{< relref "./Rule-1-1-1" >}})
* [Rule 1.1.2]({{< relref "./Rule-1-1-2" >}})
* [Rule 1.1.3]({{< relref "./Rule-1-1-3" >}})
* [Rule 1.1.4]({{< relref "./Rule-1-1-4" >}})
* [Rule 1.1.5]({{< relref "./Rule-1-1-5" >}})
* [Rule 1.1.6]({{< relref "./Rule-1-1-6" >}})
* [Rule 1.1.7]({{< relref "./Rule-1-1-7" >}})
* [Rule 1.1.8]({{< relref "./Rule-1-1-8" >}})

## Criterion 1.2

> Chaque
> [image de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration)
> est-elle correctement ignorée par les technologies d’assistance ?

* [Rule 1.2.1]({{< relref "./Rule-1-2-1" >}})
* [Rule 1.2.2]({{< relref "./Rule-1-2-2" >}})
* [Rule 1.2.3]({{< relref "./Rule-1-2-3" >}})
* [Rule 1.2.4]({{< relref "./Rule-1-2-4" >}})
* [Rule 1.2.5]({{< relref "./Rule-1-2-5" >}})
* [Rule 1.2.6]({{< relref "./Rule-1-2-6" >}})

## Criterion 1.3

> Pour chaque image
> [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information)
> ayant une
> [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image),
> cette alternative est-elle pertinente (hors cas particuliers) ?

* [Rule 1.3.1]({{< relref "./Rule-1-3-1" >}})
* [Rule 1.3.2]({{< relref "./Rule-1-3-2" >}})
* [Rule 1.3.3]({{< relref "./Rule-1-3-3" >}})
* [Rule 1.3.4]({{< relref "./Rule-1-3-4" >}})
* [Rule 1.3.5]({{< relref "./Rule-1-3-5" >}})
* [Rule 1.3.6]({{< relref "./Rule-1-3-6" >}})
* [Rule 1.3.7]({{< relref "./Rule-1-3-7" >}})
* [Rule 1.3.8]({{< relref "./Rule-1-3-8" >}})
* [Rule 1.3.9]({{< relref "./Rule-1-3-9" >}})

## Criterion 1.4

> Pour chaque image utilisée comme
> [CAPTCHA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#captcha) ou comme
> [image-test](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-test), ayant une
> [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image),
> cette alternative permet-elle d’identifier la nature et la fonction de l’image ?

* [Rule 1.4.1]({{< relref "./Rule-1-4-1" >}})
* [Rule 1.4.2]({{< relref "./Rule-1-4-2" >}})
* [Rule 1.4.3]({{< relref "./Rule-1-4-3" >}})
* [Rule 1.4.4]({{< relref "./Rule-1-4-4" >}})
* [Rule 1.4.5]({{< relref "./Rule-1-4-5" >}})
* [Rule 1.4.6]({{< relref "./Rule-1-4-6" >}})
* [Rule 1.4.7]({{< relref "./Rule-1-4-7" >}})

## Criterion 1.5

> Pour chaque image utilisée comme
> [CAPTCHA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#captcha), une solution
> d’accès alternatif au contenu ou à la fonction du CAPTCHA est-elle présente ?

* [Rule 1.5.1]({{< relref "./Rule-1-5-1" >}})
* [Rule 1.5.2]({{< relref "./Rule-1-5-2" >}})

## Criterion 1.6

> Chaque image
> [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information)
> a-t-elle, si nécessaire, une
> [description détaillée](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#description-detaillee-image)
> ?

* [Rule 1.6.1]({{< relref "./Rule-1-6-1" >}})
* [Rule 1.6.2]({{< relref "./Rule-1-6-2" >}})
* [Rule 1.6.3]({{< relref "./Rule-1-6-3" >}})
* [Rule 1.6.4]({{< relref "./Rule-1-6-4" >}})
* [Rule 1.6.5]({{< relref "./Rule-1-6-5" >}})
* [Rule 1.6.6]({{< relref "./Rule-1-6-6" >}})
* [Rule 1.6.7]({{< relref "./Rule-1-6-7" >}})
* [Rule 1.6.8]({{< relref "./Rule-1-6-8" >}})
* [Rule 1.6.9]({{< relref "./Rule-1-6-9" >}})
* [Rule 1.6.10]({{< relref "./Rule-1-6-10" >}})

## Criterion 1.7

> Pour chaque image
> [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information)
> ayant une
> [description détaillée](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#description-detaillee-image),
> cette description est-elle pertinente ?

* [Rule 1.7.1]({{< relref "./Rule-1-7-1" >}})
* [Rule 1.7.2]({{< relref "./Rule-1-7-2" >}})
* [Rule 1.7.3]({{< relref "./Rule-1-7-3" >}})
* [Rule 1.7.4]({{< relref "./Rule-1-7-4" >}})
* [Rule 1.7.5]({{< relref "./Rule-1-7-5" >}})
* [Rule 1.7.6]({{< relref "./Rule-1-7-6" >}})

## Criterion 1.8

> Chaque
> [image texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-texte)
> [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information),
> en l’absence d’un
> [mécanisme de remplacement](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#mecanisme-de-remplacement),
> doit si possible être remplacée par du
> [texte stylé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#texte-style). Cette
> règle est-elle respectée (hors cas particuliers) ?

* [Rule 1.8.1]({{< relref "./Rule-1-8-1" >}})
* [Rule 1.8.2]({{< relref "./Rule-1-8-2" >}})
* [Rule 1.8.3]({{< relref "./Rule-1-8-3" >}})
* [Rule 1.8.4]({{< relref "./Rule-1-8-4" >}})
* [Rule 1.8.5]({{< relref "./Rule-1-8-5" >}})
* [Rule 1.8.6]({{< relref "./Rule-1-8-6" >}})

## Criterion 1.9

> Chaque
> [légende d’image](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) est-elle,
> si nécessaire, correctement reliée à l’image correspondante ?

* [Rule 1.9.1]({{< relref "./Rule-1-9-1" >}})
* [Rule 1.9.2]({{< relref "./Rule-1-9-2" >}})
* [Rule 1.9.3]({{< relref "./Rule-1-9-3" >}})
* [Rule 1.9.4]({{< relref "./Rule-1-9-4" >}})
* [Rule 1.9.5]({{< relref "./Rule-1-9-5" >}})


---
title: "Rule 1.7.3"
date: 2024-08-11T11:54:33+02:00
weight: 8
---

## Summary

No-check rule

## Business description

### Criterion

[1.7](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.7)

### Test

[1.7.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.7.3)

### Description

> Chaque
> [image objet](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-objet)
> (balise `<object>` avec l’attribut `type="image/…"`)
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> , ayant une
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> , vérifie-t-elle ces conditions?
>
> * La
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> dans la page et signalée par l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> est pertinente
> * La
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente à l’
> [image objet](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-objet)
> est pertinente
> * La
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> via un
> [lien ou un bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> est pertinente
> * Le passage de texte associé via l’attribut WAI-ARIA `aria-describedby` est pertinent.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 1.7.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010703/)
* [Unit test file for rule 1.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010703Test.java)
* [Class file for rule 1.7.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010703.java)



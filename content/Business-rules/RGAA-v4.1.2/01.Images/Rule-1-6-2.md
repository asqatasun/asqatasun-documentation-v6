---
title: "Rule 1.6.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[1.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.6)

### Test

[1.6.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.6.2)

### Description

> Chaque
> [image objet](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-objet)
> (balise `<object>` avec l’attribut `type="image/…"`)
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> , qui nécessite une
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> , vérifie-t-elle une de ces conditions?
>
> * Il existe un attribut `longdesc` qui donne l’adresse (URL) d’une page ou d’un emplacement dans la page contenant la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> * Il existe une
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> contenant la référence à une
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente à l’image
> * Il existe un
> [lien ou un bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> permettant d’accéder à la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> .


#### Notes techniques (1.6)

> Dans le cas du SVG, le manque de support de l’élément `<title>` et `<desc>` par les technologies d’assistance crée
> une difficulté dans le cas de l’implémentation de l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> de l’image et de sa
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> . Dans ce cas, il est recommandé d’utiliser l’attribut WAI-ARIA `aria-label` pour implémenter à la fois l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> courte et la référence à la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente ou l’attribut WAI-ARIA `aria-labelledby` pour associer les passages de texte faisant office d’alternative
> courte et de
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> .
> L’utilisation de l’attribut WAI-ARIA aria-describedby n’est pas recommandée pour lier une image (`<img>`, `<object>`,
> `<embed>`, `<canvas>`) à sa
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> , par manque de support des technologies d’assistance. Néanmoins, lorsqu’il est utilisé, l’attribut devra
> nécessairement faire référence à l’`id` de la zone contenant la
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> .
> La
> [description détaillée](https://accessibilite.numerique.gouv.fr/methode/glossaire/#description-detaillee-image)
> adjacente peut être implémentée via une balise `<figcaption>`, dans ce cas le critère 1.9 doit être vérifié
> (utilisation de `<figure>` et des attributs WAI-ARIA `role="figure"` et `aria-label`, notamment).
> L'attribut `longdesc` qui constitue une des conditions du test 1.6.1 (et dont la pertinence est vérifiée avec le test
> 1.7.1) est désormais considéré comme obsolète par la spécification HTML en cours. La vérification de cet attribut ne
> sera donc requise que pour les versions de la spécification HTML antérieure à HTML 5.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 1.6.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010602/)
* [Unit test file for rule 1.6.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010602Test.java)
* [Class file for rule 1.6.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010602.java)



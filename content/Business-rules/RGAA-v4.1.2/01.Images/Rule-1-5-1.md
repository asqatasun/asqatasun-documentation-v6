---
title: "Rule 1.5.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[1.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.5)

### Test

[1.5.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.5.1)

### Description

> Chaque image (balises `<img>`, `<area>`, `<object>`, `<embed>`, `<svg>`, `<canvas>` ou possédant un attribut WAI-ARIA
> `role="img"`) utilisée comme
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> vérifie-t-elle une de ces conditions?
>
> * Il existe une autre forme de
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> non graphique, au moins
> * Il existe une autre solution d’accès à la fonctionnalité qui est sécurisée par le
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> .

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 1.5.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010501/)
* [Unit test file for rule 1.5.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010501Test.java)
* [Class file for rule 1.5.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010501.java)



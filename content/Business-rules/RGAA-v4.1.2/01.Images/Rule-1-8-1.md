---
title: "Rule 1.8.1"
date: 2024-08-11T11:54:33+02:00
weight: 8
---

## Summary

No-check rule

## Business description

### Criterion

[1.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.8)

### Test

[1.8.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.8.1)

### Description

> Chaque
> [image texte](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-texte)
> (balise `<img>` ou possédant un attribut WAI-ARIA `role="img"`)
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> , en l’absence d’un
> [mécanisme de remplacement](https://accessibilite.numerique.gouv.fr/methode/glossaire/#mecanisme-de-remplacement)
> , doit si possible être remplacée par du
> [texte stylé](https://accessibilite.numerique.gouv.fr/methode/glossaire/#texte-style)
> . Cette règle est-elle respectée (hors cas particuliers) ?


#### Notes techniques (1.8)

> Le texte dans les images vectorielles étant du texte réel, il n’est pas concerné par ce critère.


#### Cas particuliers (1.8)

> Pour ce critère, il existe une gestion de cas particulier lorsque le texte fait partie du logo, d’une dénomination
> commerciale, d’un
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> , d’une
> [image-test](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-test)
> ou d’une image dont l’exactitude graphique serait considérée comme essentielle à la bonne transmission de
> l’information véhiculée par l’image. Dans ces situations, le critère est non applicable pour ces éléments.

### Level

AA


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 1.8.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010801/)
- [Unit test file for rule 1.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010801Test.java)
- [Class file for rule 1.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010801.java)



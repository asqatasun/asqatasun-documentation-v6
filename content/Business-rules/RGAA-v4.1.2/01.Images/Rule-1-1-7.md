---
title: "Rule 1.1.7"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in checking whether each `<embed>` tag with a `type="image/…"` attribute, carrying information, owns
a textual alternative.

## Business description

### Criterion

[1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.1)

### Test

[1.1.7](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.1.7)

### Description

> Chaque image embarquée (balise `<embed>` avec l’attribut `type="image/…"`)
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> , vérifie-t-elle une de ces conditions?
>
> * La balise `<embed>` possède une
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> et un attribut `role="img"`
> * L’élément `<embed>` est immédiatement suivi d’un
> [lien ou bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> permettant d’accéder à un
> [contenu alternatif](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contenu-alternatif)
> * Un mécanisme permet à l’utilisateur de remplacer l’élément `<embed>` par un
> [contenu alternatif](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contenu-alternatif)
> .

### Level

A

## Technical description

### Scope

Page

### Decision level

Decidable with marker

## Algorithm

### Selection

#### Set1

All the `<embed>` tags with a `type="image"` attribute, not within a link and not identified as captcha (see Notes about
captcha detection)

#### Set2

All the elements of **Set1** identified as informative image by marker usage (see Notes for details about detection
through marker)

#### Set3

All the elements of **Set1** identified neither as informative image, nor as decorative image by marker usage (see Notes
for details about detection through marker)

### Process

#### Test1

For each element of **Set2**, check the presence of the `"role"` attribute with a value equals to `"img"`

For each occurrence of false-result of **Test1**, raise a MessageA.

#### Test2

For each element of **Set2**, check whether the tag owns a textual alternative (see Notes for details about textual
alternative detection).

For each occurrence of false-result of **Test2**, raise a MessageB.

#### Test3

For each element of **Set3**, check whether the tag owns a textual alternative (see Notes for details about textual
alternative detection).

For each occurrence of true-result of **Test2**, raise a MessageC.

For each occurrence of false-result of **Test2**, raise a MessageD.

#### Messages

##### MessageA

* code : **AriaImgRoleMissing**
* status: Failed
* parameter : `"title"` attribute, `"aria-label"` attribute, `"computed accessible name"`, `"data"` attribute, tag name
* present in source : yes

##### MessageB

* code : **CheckPresenceOfAlternativeMechanismForInformativeImage**
* status: Pre-Qualified
* parameter : `"title"` attribute, `"aria-label"` attribute, `"computed accessible name"`, `"src"` attribute
* present in source : yes

##### MessageC

* code : **CheckNatureOfElementWithTextualAlternative**
* status: Pre-Qualified
* parameter : `"title"` attribute, `"aria-label"` attribute, `"computed accessible name"`, `"src"` attribute
* present in source : yes

##### MessageD

* code : **CheckNatureOfElementWithoutTextualAlternative**
* status: Pre-Qualified
* parameter : `"title"` attribute, `"aria-label"` attribute, `"computed accessible name"`, `"src"` attribute
* present in source : yes

### Analysis

#### Not Applicable

The page has no `<embed>` tag with a `type="image"` attribute (**Set1** is empty)

#### Passed

All `<embed>` tags with a `type="image"` attribute are identified as informative, have a textual alternative and a
role attribute equals to `image`
(**Test1** and **Test2** return true for all the elements of **Set2** and **Set3** is empty).

#### Failed

At least one `<embed>` tag with a `type="image"` attribute, identified as informative, doesn't have a role attribute
equals to `image` (**Test1** returns false for at least one element of **Set2**).

#### Pre-qualified

In all other cases

## Notes

### Textual alternative detection

The textual alternative can be set by the presence of any the following elements :

1. Text associated via the `aria-labelledby` WAI-ARIA attribute
2. Presence of an `aria-label` WAI-ARIA attribute
3. Presence of a `title` attribute

That order has to be respected to compute the textual alternative.

For instance, if an `aria-label` WAI-ARIA attribute and an `title` attribute are both present, the content of the
`aria-label` WAI-ARIA attribute is considered as the textual alternative.

If none of these elements are present, two other conditions exist to set the textual alternative :

* A link, or a button is present just before or just after the element
* A mechanism exists to let the user replace the element by an alternative content.

### Markers

**Informative images** markers are set through the **INFORMATIVE_IMAGE_MARKER** parameter.

**Decorative images** markers are set through the **DECORATIVE_IMAGE_MARKER** parameter.

The value(s) passed as marker(s) will be checked against the following attributes:

* `class`
* `id`
* `role`

### Captcha detection

An element is identified as a CAPTCHA when the "captcha" occurrence is found :

* on one attribute of the element
* or within the text of the element
* or on one attribute of one parent of the element
* or within the text of one parent of the element
* or on one attribute of a sibling of the element
* or within the text of a sibling of the element

## Files

* [TestCases files for rule 1.1.7](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010107/)
* [Unit test file for rule 1.1.7](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010107Test.java)
* [Class file for rule 1.1.7](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010107.java)



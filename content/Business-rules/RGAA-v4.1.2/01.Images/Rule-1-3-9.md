---
title: "Rule 1.3.9"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[1.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.3)

### Test

[1.3.9](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#1.3.9)

### Description

> Pour chaque image
> [porteuse d’information](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-porteuse-d-information)
> et ayant une
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> , l’
> [alternative textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)
> est-elle
> [courte et concise](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-courte-et-concise)
> (hors cas particuliers) ?


#### Cas particuliers (1.3)

> Il existe une gestion de cas particuliers lorsque l’image est utilisée comme
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> ou comme
> [image-test](https://accessibilite.numerique.gouv.fr/methode/glossaire/#image-test)
> . Dans cette situation, où il n’est pas possible de donner une alternative pertinente sans détruire l’objet du
> CAPTCHA ou du test, le critère est non applicable.
> Note le cas des CAPTCHA et des images-test est traité de manière spécifique par le critère 1.4.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 1.3.9](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule010309/)
- [Unit test file for rule 1.3.9](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010309Test.java)
- [Class file for rule 1.3.9](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule010309.java)



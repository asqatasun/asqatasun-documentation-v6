---
title: "Rule 4.1.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in detecting all the links allowing to download a time-based media file and all the tags allowing to
display a time-based media.

## Business description

### Criterion

[4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.1)

### Test

[4.1.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.1.3)

### Description

> Chaque
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> synchronisé pré-enregistré vérifie-t-il, si nécessaire, une de ces conditions (hors cas particuliers)?
>
> * Il existe une
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> accessible via un
> [lien ou bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> * Il existe une
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> adjacente clairement identifiable
> * Il existe une
> [audiodescription](https://accessibilite.numerique.gouv.fr/methode/glossaire/#audiodescription-synchronisee-media-temporel)
> synchronisée
> * Il existe une version alternative avec une
> [audiodescription](https://accessibilite.numerique.gouv.fr/methode/glossaire/#audiodescription-synchronisee-media-temporel)
> synchronisée accessible via un
> [lien ou bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> .


#### Cas particuliers (4.1)

> Il existe une gestion de cas particulier lorsque
>
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé à des fins décoratives (c’est-à-dire qu’il n’apporte aucune information)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est lui-même une alternative à un contenu de la page (une vidéo en langue des signes ou la vocalisation d’un texte,
> par exemple)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé pour accéder à une version agrandie
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé comme un
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> fait partie d’un test qui deviendrait inutile si la
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> , les
> [sous-titres synchronisés](https://accessibilite.numerique.gouv.fr/methode/glossaire/#sous-titres-synchronises-objet-multimedia)
> ou l’
> [audiodescription](https://accessibilite.numerique.gouv.fr/methode/glossaire/#audiodescription-synchronisee-media-temporel)
> étaient communiqués
> * Pour les services de l’État, les collectivités territoriales et leurs établissements si le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> a été publié entre le 23 septembre 2019 et le 23 septembre 2020 sur un site internet, intranet ou extranet créé
> depuis le 23 septembre 2018, il est exempté de l’obligation d’accessibilité
> * Pour les personnes de droit privé mentionnées aux 2° à 4° du I de l’article 47 de la loi du 11 février 2005 si le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> a été publié avant le 23 septembre 2020, il est exempté de l’obligation d’accessibilité.
> Dans ces situations, le critère est non applicable.
> Ce cas particulier s’applique également aux critères 4.2, 4.3, 4.5.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

* All links to download a time-based media file
* AND all the following tags:
    * `<audio>`
    * `<bgsound>`
    * `<video>`
    * `<object>`
    * `<embed>`
    * `<svg>`
    * `<canvas>`

css selector :

```css
audio[src],
audio:has(source[src]),
bgsound,
video[src],
video:has(source[src]),
object[data],
embed[src],
svg,
canvas,
a[href~=(?i)\.(mp4|avi|wmv|mov|Xvid|mkv|mka|mks|FLV|rmvb|MPA|WMA|MP2|M2P|DIF|DV|VOB|VRO|rmvb|vivo|bik|ASF|ifo|mts|mxf|nds|rv|web|wlmp|wmp|ogv)]
a[href~=(?i)\.(WAV|CDA|MID|MP2|MP3|mp3PRO|MOD|RM|RAM|WMA|Ogg|oga|AIF|AIFF|AA|AAC|M4A|VQF|AU|M3U|RIFF|BWF|CAF|PCM|RAW|FLAC|ALAC|AC3|ACC)]
```

### Process

#### Test1

Test whether **Set1** is not empty. If yes, raise a MessageA.

##### MessageA : Check manually the elements of the scope

* code: ManualCheckOnElements
* status: Pre-qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

* The page has no tags allowing to display a time-based media
* The page has no link to download a time-based media file.

#### Pre-qualified

In all other cases


## Files

* [TestCases files for rule 4.1.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule040103/)
* [Unit test file for rule 4.1.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040103Test.java)
* [Class file for rule 4.1.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040103.java)



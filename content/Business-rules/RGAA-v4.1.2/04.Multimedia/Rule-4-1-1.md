---
title: "Rule 4.1.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in detecting all the links allowing to download a audio file and all the tags allowing to play a
audio file.

## Business description

### Criterion

[4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.1)

### Test

[4.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.1.1)

### Description

> Chaque
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> pré-enregistré seulement audio, vérifie-t-il, si nécessaire, l’une de ces conditions (hors cas particuliers)?
>
> * Il existe une
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> accessible via un
> [lien ou bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> * Il existe une
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> adjacente clairement identifiable.


#### Cas particuliers (4.1)

> Il existe une gestion de cas particulier lorsque
>
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé à des fins décoratives (c’est-à-dire qu’il n’apporte aucune information)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est lui-même une alternative à un contenu de la page (une vidéo en langue des signes ou la vocalisation d’un texte,
> par exemple)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé pour accéder à une version agrandie
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> est utilisé comme un
> [CAPTCHA](https://accessibilite.numerique.gouv.fr/methode/glossaire/#captcha)
> * Le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> fait partie d’un test qui deviendrait inutile si la
> [transcription textuelle](https://accessibilite.numerique.gouv.fr/methode/glossaire/#transcription-textuelle-media-temporel)
> , les
> [sous-titres synchronisés](https://accessibilite.numerique.gouv.fr/methode/glossaire/#sous-titres-synchronises-objet-multimedia)
> ou l’
> [audiodescription](https://accessibilite.numerique.gouv.fr/methode/glossaire/#audiodescription-synchronisee-media-temporel)
> étaient communiqués
> * Pour les services de l’État, les collectivités territoriales et leurs établissements si le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> a été publié entre le 23 septembre 2019 et le 23 septembre 2020 sur un site internet, intranet ou extranet créé
> depuis le 23 septembre 2018, il est exempté de l’obligation d’accessibilité
> * Pour les personnes de droit privé mentionnées aux 2° à 4° du I de l’article 47 de la loi du 11 février 2005 si le
> [média temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> a été publié avant le 23 septembre 2020, il est exempté de l’obligation d’accessibilité.
> Dans ces situations, le critère est non applicable.
> Ce cas particulier s’applique également aux critères 4.2, 4.3, 4.5.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

* All links to download a audio file
* AND all the following tags:
    * `<audio>`
    * `<bgsound>`
    * `<video>` with audio file
    * `<object>`
    * `<embed>`

css selector :

```css
audio[src],
audio:has(source[src]),
bgsound,
audio[src~=(?i)\.(WAV|CDA|MID|MP2|MP3|mp3PRO|MOD|RM|RAM|WMA|Ogg|oga|AIF|AIFF|AA|AAC|M4A|VQF|AU|M3U|RIFF|BWF|CAF|PCM|RAW|FLAC|ALAC|AC3|ACC)] ,
audio:has(source[src~=(?i)\.(WAV|CDA|MID|MP2|MP3|mp3PRO|MOD|RM|RAM|WMA|Ogg|oga|AIF|AIFF|AA|AAC|M4A|VQF|AU|M3U|RIFF|BWF|CAF|PCM|RAW|FLAC|ALAC|AC3|ACC)]) ,
object[data],
embed[src],
a[href~=(?i)\.(WAV|CDA|MID|MP2|MP3|mp3PRO|MOD|RM|RAM|WMA|Ogg|oga|AIF|AIFF|AA|AAC|M4A|VQF|AU|M3U|RIFF|BWF|CAF|PCM|RAW|FLAC|ALAC|AC3|ACC)]
```

### Process

#### Test1

Test whether **Set1** is not empty. If yes, raise a MessageA.

##### MessageA : Check manually the elements of the scope

* code: ManualCheckOnElements
* status: Pre-qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

* The page has no `<audio>`, `<bgsound>`, `<video>` (with audio file), `<object>` or `<embed>` tag.
* The page has no link to download a audio file.

#### Pre-qualified

In all other cases


## Files

* [TestCases files for rule 4.1.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule040101/)
* [Unit test file for rule 4.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040101Test.java)
* [Class file for rule 4.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040101.java)



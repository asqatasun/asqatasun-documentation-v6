---
title: "Rule 4.8.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in detecting all the tags for no time-based media.

## Business description

### Criterion

[4.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.8)

### Test

[4.8.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#4.8.1)

### Description

> Chaque
> [média non temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-non-temporel)
> vérifie-t-il, si nécessaire, une de ces conditions (hors cas particuliers)?
>
> * Un
> [lien ou un bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> , clairement identifiable, permet d’accéder à une page contenant une alternative
> * Un
> [lien ou un bouton adjacent](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-ou-bouton-adjacent)
> , clairement identifiable, permet d’accéder à une alternative dans la page.


#### Cas particuliers (4.8)

> Il existe une gestion de cas particulier lorsque
>
> * Le
> [média non temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-non-temporel)
> est utilisé à des fins décoratives (c’est-à-dire qu’il n’apporte aucune information)
> * Le
> [média non temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-non-temporel)
> est diffusé dans un
> [environnement maîtrisé](https://accessibilite.numerique.gouv.fr/methode/glossaire/#environnement-maitrise)
> * Le
> [média non temporel](https://accessibilite.numerique.gouv.fr/methode/glossaire/#media-non-temporel)
> est inséré via JavaScript en vérifiant la présence et la version du plug-in, en remplacement d’un
> [contenu alternatif](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contenu-alternatif)
> déjà présent.
> Dans ces situations, le critère est non applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

* All the following tags:
    * `<svg>`
    * `<canvas>`
    * `<object>`
    * `<embed>`

css selector :

```css
svg,
canvas,
object[data],
embed[src]
```

### Process

#### Test1

Test whether **Set1** is not empty. If yes, raise a MessageA.

##### MessageA : Check manually the elements of the scope

* code: ManualCheckOnElements
* status: Pre-qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

The page has no `<svg>`, `<canvas>`, `<object>` or `<embed>` tag.

#### Pre-qualified

In all other cases


## Files

* [TestCases files for rule 4.8.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule040801/)
* [Unit test file for rule 4.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040801Test.java)
* [Class file for rule 4.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule040801.java)


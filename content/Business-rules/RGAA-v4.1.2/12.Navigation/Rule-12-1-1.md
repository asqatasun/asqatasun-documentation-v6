---
title: "Rule 12.1.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test is of site scope or group of pages scope. When requesting a page audit, the result is automatically set to
"Not Applicable". No check is done instead.

## Business description

### Criterion

[12.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#12.1)

### Test

[12.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#12.1.1)

### Description

> Chaque
> [ensemble de pages](https://accessibilite.numerique.gouv.fr/methode/glossaire/#ensemble-de-pages)
> vérifie-t-il une de ces conditions (hors cas particuliers)?
>
> * Un
> [menu de navigation](https://accessibilite.numerique.gouv.fr/methode/glossaire/#menu-et-barre-de-navigation)
> et un
> [plan du site](https://accessibilite.numerique.gouv.fr/methode/glossaire/#page-plan-du-site)
> sont présents
> * Un
> [menu de navigation](https://accessibilite.numerique.gouv.fr/methode/glossaire/#menu-et-barre-de-navigation)
> et un
> [moteur de recherche](https://accessibilite.numerique.gouv.fr/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web)
> sont présents
> * Un
> [moteur de recherche](https://accessibilite.numerique.gouv.fr/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web)
> et un
> [plan du site](https://accessibilite.numerique.gouv.fr/methode/glossaire/#page-plan-du-site)
> sont présents.


#### Cas particuliers (12.1)

> Il existe une gestion de cas particulier lorsque le site web est constitué d’une seule page ou d’un nombre très
> limité de pages (cf. note). Dans ce cas-là, le critère est non applicable.
> Le critère est également non applicable pour les pages d’un ensemble de pages qui sont le résultat ou une partie d’un
> processus (un processus de paiement ou de prise de commande, par exemple).
> Note l’appréciation d’un nombre très limité de pages devrait être réservé à un site dont l’ensemble des pages sont
> atteignables depuis la page d’accueil.

### Level

AA


## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Applicable

Page audit

#### Not Tested

Group of pages audit or site audit


## Files

* [TestCases files for rule 12.1.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule120101/)
* [Unit test file for rule 12.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule120101Test.java)
* [Class file for rule 12.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule120101.java)


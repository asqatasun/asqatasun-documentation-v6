---
title: "Rule 12.9.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[12.9](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#12.9)

### Test

[12.9.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#12.9.1)

### Description

> Dans chaque page web, chaque
> [élément recevant le focus](https://accessibilite.numerique.gouv.fr/methode/glossaire/#prise-de-focus)
> vérifie-t-il une de ces conditions?
>
> * Il est possible d’atteindre l’élément suivant ou précédent pouvant recevoir le focus avec la touche de tabulation
> * L’utilisateur est informé d’un mécanisme fonctionnel permettant d’atteindre au clavier l’élément suivant ou
> précédent pouvant recevoir le focus.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 12.9.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule120901/)
* [Unit test file for rule 12.9.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule120901Test.java)
* [Class file for rule 12.9.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule120901.java)



---
title: "Type of Result by Rule"
date: 2020-11-12T17:33:10+01:00
---

## Introduction

We call a *rule* the implementation of a test. For instance **Rule RGAAv4 5.1.1** is the implementation of
[test RGAAv4 5.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-5-1-1)

In Asqatasun, a rule may produce up to
[5 kinds of results](../../../User/5-types-of-result/). In the audit parameters, the user may provide
[markers](../../../User/HTML-markers/) to enlarge the scope and accuracy of the audit. Depending on the audited webpage,
and the presence of markers, a rule can produce up to 4 kinds of result.

We call a rule *decidable* when, in whatever circumstance, the produced results are included in Passed or Failed or Not
Applicable.

Here we list in an extensive way, which one(s) of the results a given rule can produce.

## Summary

Since Asqatasun 5.0.0:

* **30** rules can produce *Passed*
* **39** rules can produce *Failed*
* **100** rules can produce *Not Applicable*
* **95** rules can produce *Pre-qualified*
* **21** rules are *decidable* (some with the help of markers)

## Download "Type of result by Rule"

* ["Type of result by Rule" Spreadsheet (OpenDocument format) 22kb](Type-of-result-by-rule.ods)

## How to gather these data

From the directory holding all design sheets of RGAAv4:

```shell script
rgrep -i "#### Passed" */Rule-*
rgrep -i "#### Failed" */Rule-*
rgrep -i "#### Not App" */Rule-*
rgrep -i -EH "#### Pre|#### NMI" */Rule-*
rgrep -i "#### Not Tested" */Rule-*
```


---
title: "Theme 6: Links"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 6.1

> Chaque
> [lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien) est-il explicite (hors
> cas particuliers) ?

* [Rule 6.1.1]({{< relref "./Rule-6-1-1" >}})
* [Rule 6.1.2]({{< relref "./Rule-6-1-2" >}})
* [Rule 6.1.3]({{< relref "./Rule-6-1-3" >}})
* [Rule 6.1.4]({{< relref "./Rule-6-1-4" >}})
* [Rule 6.1.5]({{< relref "./Rule-6-1-5" >}})

## Criterion 6.2

> Dans chaque page web, chaque
> [lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien) a-t-il un
> [intitulé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
> ?

* [Rule 6.2.1]({{< relref "./Rule-6-2-1" >}})


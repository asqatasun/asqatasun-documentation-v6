---
title: "Rule 6.2.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in checking that there are no empty links in the page

## Business description

### Criterion

[6.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.2)

### Test

[6.2.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.2.1)

### Description

> Dans chaque page web, chaque
> [lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien)
> a-t-il un
> [intitulé](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
> entre `<a>` et `</a>` ?


#### Notes techniques (6.2)

> Une ancre n’est pas un lien même si pendant longtemps l’élément `<a>` a servi de support à cette technique. Elle
> n’est donc pas concernée par le présent critère.

### Level

A

## Technical description

### Scope

Page

### Decision level

Decidable

## Algorithm

### Selection

#### Set1

All `<a>` tags with a `"href"` attribute that have neither an `"aria-label"` attribute nor an `"aria-described"`
attribute.
All the tags with a `"role"` attribute equals to "link" that have neither an `"aria-label"` attribute nor an
`"aria-described"` attribute.

### Process

#### Test1

For each element of **Set1**, we check whether the link text is not empty.

For each element returning true in **Test1**, raise a MessageA

##### MessageA : Empty Link

* code : EmptyLink
* status: Failed
* parameter : computed accessible name, `"title"` attribute, snippet
* present in source : yes

### Analysis

#### Not Applicable

The page has no link (**Set1** is empty)

#### Passed

All the links of the page have not empty textual context (**Test1** returns false for all the elements of **Set1**)

#### Failed

At least one link have an empty textual context (**Test1** returns false for at least one element of **Set1**)

## Notes

We exclude tags with an aria-label attribute or an aria-described attribute as we consider that these tags have
an alternative.

For this test, the `"title"` attribute is not taken into account to compute the textual content of the link. It ensures
empty links only with a `"title"` attribute are seen as false.

## Files

- [TestCases files for rule 6.2.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule060201/)
- [Unit test file for rule 6.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060201Test.java)
- [Class file for rule 6.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060201.java)



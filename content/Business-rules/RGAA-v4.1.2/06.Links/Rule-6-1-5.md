---
title: "Rule 6.1.5"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[6.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.1)

### Test

[6.1.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.1.5)

### Description

> Pour chaque
> [lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien)
> ayant un
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> , le
> [nom accessible du lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
> contient-il au moins l’
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> (hors cas particuliers) ?


#### Notes techniques (6.1)

> Lorsque l’intitulé visible est complété par une autre expression dans le nom accessible
>
> * WCAG insiste sur le placement de l’intitulé visible au début du nom accessible sans toutefois réserver
> l’exclusivité de cet emplacement
> * WCAG considère comme un cas d’échec une correspondance non exacte de la chaîne de caractères de l’intitulé visible
> au sein du nom accessible.
> Par exemple, si l’on considère l’intitulé visible «Commander maintenant» complété dans le nom accessible par
> l’expression «produit X», on peut avoir les différents cas suivants
>
> * «Commander maintenant produit X» est valide (bonne pratique)
> * «Produit X : commander maintenant» est valide
> * «Commander produit X maintenant» est non valide.


#### Cas particuliers (6.1)

> Il existe une gestion de cas particuliers pour les tests 6.1.1, 6.1.2, 6.1.3 et 6.1.4 lorsque le lien est
> [ambigu pour tout le monde](https://accessibilite.numerique.gouv.fr/methode/glossaire/#ambigu-pour-tout-le-monde)
> . Dans cette situation, où il n’est pas possible de rendre le lien explicite dans son contexte, le critère est non
> applicable.
> Il existe une gestion de cas particuliers pour le test 6.1.5 lorsque
>
> * La ponctuation et les lettres majuscules sont présentes dans le texte de l’
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> elles peuvent être ignorées dans le nom accessible sans porter à conséquence
> * Le texte de l’
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> sert de symbole le texte ne doit pas être interprété littéralement au niveau du nom accessible. Le nom doit exprimer
> la fonction véhiculée par le symbole (par exemple, “B” au niveau d’un éditeur de texte aura pour nom accessible
> “Mettre en gras”, le signe “>” en fonction du contexte signifiera “Suivant” ou “Lancer la vidéo”). Le cas des
> symboles mathématiques fait cependant exception (voir la note ci-dessous).
> Note si l’étiquette visible représente une expression mathématique, les symboles mathématiques peuvent être repris
> littéralement pour servir d’étiquette au nom accessible (ex. “A>B”). Il est laissé à l’utilisateur le soin d’opérer
> la correspondance entre l’expression et ce qu’il doit épeler compte tenu de la connaissance qu’il a du fonctionnement
> de son logiciel de saisie vocale (“A plus grand que B” ou “A supérieur à B”).

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 6.1.5](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule060105/)
* [Unit test file for rule 6.1.5](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060105Test.java)
* [Class file for rule 6.1.5](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060105.java)



---
title: "Rule 6.1.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in checking whether each image link is explicit enough to understand the purpose and the target.

## Business description

### Criterion

[6.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.1)

### Test

[6.1.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#6.1.2)

### Description

> Chaque
> [lien image](https://accessibilite.numerique.gouv.fr/methode/glossaire/#lien-image)
> vérifie-t-il une de ces conditions (hors cas particuliers)?
>
> * L’
> [intitulé de lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
> seul permet d’en comprendre la fonction et la destination
> * L’
> [intitulé de lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
> additionné au
> [contexte du lien](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contexte-du-lien)
> permet d’en comprendre la fonction et la destination.


#### Notes techniques (6.1)

> Lorsque l’intitulé visible est complété par une autre expression dans le nom accessible
>
> * WCAG insiste sur le placement de l’intitulé visible au début du nom accessible sans toutefois réserver
> l’exclusivité de cet emplacement
> * WCAG considère comme un cas d’échec une correspondance non exacte de la chaîne de caractères de l’intitulé visible
> au sein du nom accessible.
> Par exemple, si l’on considère l’intitulé visible «Commander maintenant» complété dans le nom accessible par
> l’expression «produit X», on peut avoir les différents cas suivants
>
> * «Commander maintenant produit X» est valide (bonne pratique)
> * «Produit X : commander maintenant» est valide
> * «Commander produit X maintenant» est non valide.


#### Cas particuliers (6.1)

> Il existe une gestion de cas particuliers pour les tests 6.1.1, 6.1.2, 6.1.3 et 6.1.4 lorsque le lien est
> [ambigu pour tout le monde](https://accessibilite.numerique.gouv.fr/methode/glossaire/#ambigu-pour-tout-le-monde)
> . Dans cette situation, où il n’est pas possible de rendre le lien explicite dans son contexte, le critère est non
> applicable.
> Il existe une gestion de cas particuliers pour le test 6.1.5 lorsque
>
> * La ponctuation et les lettres majuscules sont présentes dans le texte de l’
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> elles peuvent être ignorées dans le nom accessible sans porter à conséquence
> * Le texte de l’
> [intitulé visible](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-visible)
> sert de symbole le texte ne doit pas être interprété littéralement au niveau du nom accessible. Le nom doit exprimer
> la fonction véhiculée par le symbole (par exemple, “B” au niveau d’un éditeur de texte aura pour nom accessible
> “Mettre en gras”, le signe “>” en fonction du contexte signifiera “Suivant” ou “Lancer la vidéo”). Le cas des
> symboles mathématiques fait cependant exception (voir la note ci-dessous).
> Note si l’étiquette visible représente une expression mathématique, les symboles mathématiques peuvent être repris
> littéralement pour servir d’étiquette au nom accessible (ex. “A>B”). Il est laissé à l’utilisateur le soin d’opérer
> la correspondance entre l’expression et ce qu’il doit épeler compte tenu de la connaissance qu’il a du fonctionnement
> de son logiciel de saisie vocale (“A plus grand que B” ou “A supérieur à B”).

### Level

A

## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

#### Set1

All the `<a>` tags with a `"href"` attribute, containing one or more image-only child tags with a non-empty alternative
(`img[alt] , object[type^=image], object[data^=data:image],"object[data$=png], object[data$=jpeg],
object[data$=jpg],object[data$=bmp], object[data$=gif], canvas, svg"`)
and all the tags with a `role` attribute equals to `link`, containing one or more image-only child tags with a
non-empty alternative
(`img[alt] , object[type^=image], object[data^=data:image],"object[data$=png], object[data$=jpeg],
object[data$=jpg],object[data$=bmp], object[data$=gif], canvas, svg"`)

The alternative of an image is determined with reference to
[the definition of an image alternative in Rgaa4.1.2](https://accessibilite.numerique.gouv.fr/methode/glossaire/#alternative-textuelle-image)

#### Set2

All the elements of **Set1** with a not empty accessible name and without context (assuming
[the definition of a link context in Rgaa4.1.2](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contexte-du-lien))
and [the definition of an accessible name in Rgaa4.1.2](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien))

#### Set3

All the elements of **Set1** with a not empty accessible name and with a context (assuming
[the definition of a link context in Rgaa4.1.2](https://accessibilite.numerique.gouv.fr/methode/glossaire/#contexte-du-lien))
and [the definition of an accessible name in Rgaa4.1.2](https://accessibilite.numerique.gouv.fr/methode/glossaire/#intitule-ou-nom-accessible-de-lien))

in other words :

size(**Set1**) = size(**Set2**) + size(**Set3**)

### Process

#### Test1

For each element of **Set2**, we check whether the computed accessible name is not pertinent (see Notes about relevancy
detection)

For each element returning true in **Test1**, raise a MessageA, raise a MessageB instead

#### Test2

For each element of **Set3**, we check whether the computed accessible name is not pertinent (see Notes about relevancy
detection)

For each element returning true in **Test2**, raise a MessageC, raise a MessageD instead

##### MessageA : Unexplicit Link

* code : UnexplicitLink
* status: Failed
* parameter : link text, `"title"` attribute, `"aria-label"` attribute, computed accessible name, snippet
* present in source : yes

##### MessageB : Check link without context pertinence

* code : CheckLinkWithoutContextPertinence
* status: Need More Info
* parameter : link text, `"title"` attribute, `"aria-label"` attribute, computed accessible name, snippet
* present in source : yes

##### MessageC : Unexplicit Link With context

* code : UnexplicitLinkWithContext
* status: Need More Info
* parameter : link text, `"title"` attribute, `"aria-label"` attribute, computed accessible name, snippet
* present in source : yes

##### MessageD : Check link with context pertinence

* code : CheckLinkWithContextPertinence
* status: Need More Info
* parameter : link text, `"title"` attribute, `"aria-label"` attribute, computed link title, snippet
* present in source : yes

### Analysis

#### Not Applicable

The page has no image link (**Set1** is empty)

#### Failed

At least one image link without context has an accessible name which is blacklisted or only composed of
non-alphanumerical characters (**Test1** returns false for at least one element)

#### Pre-qualified

In all other cases

## Notes

We assume here that the links have children and all of them are of image type.

Area tags as child tags are not taken into account, even if the rule mentions them, because the html specification
forbids it.

The accessible name is computed regarding the following rules :

* Text associated through the usage of the WAI-ARIA `aria-labelledby` attribute
* Otherwise, content of the WAI-ARIA `aria-label` attribute
* Otherwise, content of the alternative of the image
* Otherwise, content de `title` attribute

All the links that have children of image type and some text or children with text are considered as composite links.

### Definition of the alternative of an image

The ‘accessible name’ for the images is obtained in the following order :

* Passage of associated text via the WAI-ARIA attribute aria-labelledby for :
    - `<img>`
    - `<svg>`
    - `<object type="image/…">`
    - `<canvas>`
    - tags with a WAI-ARIA `role="img"` attribute.
* Otherwise, content of the WAI-ARIA attribute aria-label for elements:
    - `<img>`
* Otherwise, content of the `title` attribute for elements:
    - `<img>`
    - `<object type="image/…">`

### Definition of not-pertinent link title

A link title is seen as not-pertinent in the following cases :

* the link title is blacklisted (regarding the
  [LinkTextBlacklist nomenclature](../Nomenclatures/LinkTextBlacklist/)
* the link only contains not alphanumerics characters


## Files

* [TestCases files for rule 6.1.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule060102/)
* [Unit test file for rule 6.1.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060102Test.java)
* [Class file for rule 6.1.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule060102.java)



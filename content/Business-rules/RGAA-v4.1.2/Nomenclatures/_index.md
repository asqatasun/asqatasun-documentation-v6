---
title: "Nomenclatures"
date: 2020-11-14T17:19:00+01:00
---

Nomenclatures are lists of texts or patterns used by rules to detect "something" and re-used through different rules.

Example: detect non-relevancy of a given content.

Asqatasun uses several nomenclatures.

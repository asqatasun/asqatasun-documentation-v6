---
title: "Rule 9.4.2"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[9.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#9.4)

### Test

[9.4.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#9.4.2)

### Description

> Dans chaque page web, chaque bloc de citation utilise-t-il une balise `<blockquote>` ?

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 9.4.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule090402/)
- [Unit test file for rule 9.4.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule090402Test.java)
- [Class file for rule 9.4.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule090402.java)



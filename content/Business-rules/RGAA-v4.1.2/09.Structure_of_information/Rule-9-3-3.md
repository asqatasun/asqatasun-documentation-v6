---
title: "Rule 9.3.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[9.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#9.3)

### Test

[9.3.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#9.3.3)

### Description

> Dans chaque page web, les informations regroupées sous forme de
> [liste](https://accessibilite.numerique.gouv.fr/methode/glossaire/#listes)
> de description utilisent-elles les balises `<dl>` et `<dt>/<dd>` ?


#### Notes techniques (9.3)

> Les attributs WAI-ARIA `role="list"` et `role="listitem"` peuvent nécessiter l’utilisation des attributs WAI-ARIA
> `aria-setsize` et `aria-posinset` dans le cas où l’ensemble de la liste n’est pas disponible via le DOM généré au
> moment de la consultation.
> Les attributs WAI-ARIA `role="tree"`, `role="tablist"`, `role="menu"`, `role="combobox"` et `role="listbox"` ne sont
> pas équivalents à une liste HTML `<ul>` ou `<ol>`.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 9.3.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule090303/)
- [Unit test file for rule 9.3.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule090303Test.java)
- [Class file for rule 9.3.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule090303.java)



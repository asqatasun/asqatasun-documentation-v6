---
title: "Rule 8.9.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

This test consists in searching patterns indicating that forbidden tags (not `div`, `span` or `table`) are used for
layout purpose.

## Business description

### Criterion

[8.9](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.9)

### Test

[8.9.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.9.1)

### Description

> Dans chaque page web les balises (à l’exception de `<div>`, `<span>` et `<table>`) ne doivent pas être utilisées
> [uniquement à des fins de présentation](https://accessibilite.numerique.gouv.fr/methode/glossaire/#uniquement-a-des-fins-de-presentation)
> . Cette règle est-elle respectée ?

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

#### Set1

All the `<a>` tags without `href`, `name` or `id` attribute and without `role` attribute whose value is `button`.

CSS selector:

```jquery-css
a:not([href])
 :not([name])
 :not([id])
 :not([role=button])
```

#### Set2

All the following tags without content (text or tag child) :

- `<p>`
- `<li>`

CSS selector:

```jquery-css
p:matchesOwn(^$):not(:has(*)):not([hidden]),
li:matchesOwn(^$):not(:has(*)):not([hidden])
```

#### Set3

All consecutive `<br>` tags without text in between.


### Process

#### Tests

##### Test1

Test emptiness of **Set1**.

- If empty, raise a MessageA
- If not empty, for each occurence of the **Set1** raise a MessageB

##### Test2

Test emptiness of **Set2**.

- If empty, raise a MessageA
- If not empty, for each occurence of the **Set2** raise a MessageC

##### Test3

Test emptiness of **Set3**.

- If empty, raise a MessageA
- If not empty, for each occurence of the **Set3** raise a MessageD

#### Messages

##### MessageA: No suspect pattern detected

- code: NoPatternDetected_Rgaa40-8-9-1
- status: Pre-Qualified
- present in source: no

##### MessageB: Link without target

- code: LinkWithoutTarget
- status: Failed
- parameter: snippet
- present in source: yes

##### MessageC: Tags without content that are used for layout purpose

- code: TagsWithoutContentUsedForLayoutPurpose
- status: Failed
- parameter: snippet
- present in source: yes

##### MessageC: Consecutive tags that are used for layout purpose

- code: consecutiveTagsUsedForLayoutPurpose
- status: Failed
- parameter: snippet
- present in source: yes


### Analysis

#### Failed

The page contains either consecutive `<br>` tags without text in between, or a tag (`<p>` or `<li>`) without content
(text or tag child), or a link (without `href`, `name` or `id` attribute and without `role` attribute whose value is
`button`).

#### Pre-qualified

In all other cases


## Notes

On latest webdev data set (2013-10-30, 78,000 pages), links without target (a:not([href]):not([name]):not([id])) have
been found on 18256 pages, i.e on 23% of the pages.

## Files

- [TestCases files for rule 8.9.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule080901/)
- [Unit test file for rule 8.9.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080901Test.java)
- [Class file for rule 8.9.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080901.java)


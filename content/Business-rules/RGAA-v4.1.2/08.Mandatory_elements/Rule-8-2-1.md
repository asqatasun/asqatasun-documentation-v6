---
title: "Rule 8.2.1"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[8.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.2)

### Test

[8.2.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.2.1)

### Description

> Pour chaque déclaration de
> [type de document](https://accessibilite.numerique.gouv.fr/methode/glossaire/#type-de-document)
> , le code source généré de la page vérifie-t-il ces conditions?
>
> * Les balises, attributs et valeurs d’attributs respectent les
> [règles d’écriture](https://accessibilite.numerique.gouv.fr/methode/glossaire/#regles-d-ecriture)
> * L’imbrication des balises est conforme
> * L’ouverture et la fermeture des balises sont conformes
> * Les valeurs d’attribut id sont uniques dans la page
> * Les attributs ne sont pas doublés sur un même élément.

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

* [TestCases files for rule 8.2.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule080201/)
* [Unit test file for rule 8.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080201Test.java)
* [Class file for rule 8.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080201.java)



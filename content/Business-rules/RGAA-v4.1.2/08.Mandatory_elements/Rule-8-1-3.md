---
title: "Rule 8.1.3"
date: 2024-08-11T11:54:33+02:00
weight: 5
---

## Summary

No-check rule

## Business description

### Criterion

[8.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.1)

### Test

[8.1.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#8.1.3)

### Description

> Pour chaque page web possédant une déclaration de
> [type de document](https://accessibilite.numerique.gouv.fr/methode/glossaire/#type-de-document)
> , celle-ci est-elle située avant la balise `<html>` dans le code source ?

### Level

A


## Technical description

### Scope

Page

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 8.1.3](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.1.2/src/test/resources/testcases/rgaa412/Rgaa412Rule080103/)
- [Unit test file for rule 8.1.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/test/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080103Test.java)
- [Class file for rule 8.1.3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.1.2/src/main/java/org/asqatasun/rules/rgaa412/Rgaa412Rule080103.java)



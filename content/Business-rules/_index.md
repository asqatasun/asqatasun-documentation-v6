---
title: "Business rules manual"
date: 2020-11-12T17:14:53+02:00
weight: 40
---

Referential insights:

* [RGAA in Asqatasun](RGAA_in_asqatasun)

Business rules manual:

* [RGAA 4.0.0]({{< relref "./RGAA-v4/" >}})
* [RGAA 4.1.2]({{< relref "./RGAA-v4.1.2/" >}})


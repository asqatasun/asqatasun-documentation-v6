---
title: "Rule 11.3.2"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test is of site scope or group of pages scope. When requesting a page audit, the result is automatically set to
"Not Applicable". No check is done instead.

## Business description

### Criterion

[11.3](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-11-3)

### Test

[11.3.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-11-3-2)

### Description

> Chaque
> [étiquette](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquette-de-champ-de-formulaire)
> associée à un
> [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
> ayant la même fonction et répétée dans un ensemble de pages est-elle
> [cohérente](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#etiquettes-coherentes) ?

### Level

AA


## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Applicable

Page audit

#### Not Tested

Group of pages audit or site audit


## Files

- [TestCases files for rule 11.3.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule110302/)
- [Unit test file for rule 11.3.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule110302Test.java)
- [Class file for rule 11.3.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule110302.java)


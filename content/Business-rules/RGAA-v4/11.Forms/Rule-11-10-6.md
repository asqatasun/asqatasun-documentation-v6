---
title: "Rule 11.10.6"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test consists in detecting `<form>` tags on the page.

The control that checks error messages has to be done manually.

## Business description

### Criterion

[11.10](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-11-10)

### Test

[11.10.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-11-10-6)

### Description

> Les messages d’erreurs fournissant une instruction ou une indication du type de données et/ou de format obligatoire
> des champs vérifient-ils une de ces conditions ?
>
> * Le message d’erreur fournissant une instruction ou une indication du type de données et/ou de format obligatoires
>     est visible et identifie le champ concerné.
> * Le champ dispose de l’attribut aria-invalid="true".

#### Particular cases (criterion 11.10)

> Le test 11.10.1 sera considéré comme non applicable lorsque le formulaire comporte un seul
> [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
> ou qu’il indique les champs optionnels de manière :
>
> * Visible ;
> * Dans la balise `<label>` ou dans la
>     [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) associée au
>     champ.
>
> Dans le cas où l’ensemble des champs d’un formulaire sont obligatoires, le test 11.10.1 reste applicable.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

All the `<form>` tags.

css selector :

```css
form
```

### Process

#### Test1

The selection handles the process.

For each occurence of the **Set1** raise a MessageA

##### MessageA: Manual check on element

* code: ManualCheckOnElements
* status: Pre-Qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

The page has no `<form>` tag (**Set1** is empty)

#### Pre-qualified

In all other cases


## Notes

We detect the elements of the scope of the test to determine whether the test is applicable.

## Files

* [TestCases files for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule111006/)
* [Unit test file for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111006Test.java)
* [Class file for rule 11.10.6](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111006.java)



---
title: "Rule 10.4.1"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

Check wether a forbidden unit is present in all CSS media types `screen`, `tv`, `handheld` and `projection`.

## Business description

### Criterion

[10.4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-10-4)

### Test

[10.4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-10-4-1)

### Description

> Dans les
> [feuilles de styles](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#feuille-de-style)
> du
> [site web](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#site-web-ensemble-de-toutes-les-pages-web),
> les unités non relatives (`pt`, `pc`, `mm`, `cm`, `in`) ne doivent pas être utilisées pour les types de média
> `screen`, `tv`, `handheld`, `projection`. Cette règle est-elle respectée (hors cas particuliers) ?

#### Particular cases (criterion 10.4)

> Dans le cas des textes en image et des sous-titres de vidéo le critère est non applicable.

### Level

AA


## Technical description

### Scope

Page

### Decision level

Decidable

## Algorithm

### Selection

#### Set1**

All the css Rules whose media is `screen`, `tv`, `handheld` and `projection`.

##### Used nomenclature

- MediaListNotAcceptingRelativeUnits

### Process

#### Test1

Check whether the properties of the css rules of **Set1** use a forbidden unit (pt, pc, mm, cm, in, defined in the
"RelativeCssUnits" nomenclature).

For each occurence returning true, raise a MessageA

#### Test2

A messageB is raised indicating that this css have to checked manually

##### MessageA : Bad Unit type

- code: BadUnitType
- status: Failed
- parameter: css-selector name
- present in source: yes

##### MessageB : Untested resource

- code: UnTestedResource
- status: Pre-Qualified
- parameter: the resource name

### Analysis

#### Passed

No property of css rules of **Set1** uses a forbidden unit (**Test1** returns false for all the elements of **Set1**)

#### Failed

At least one css-rule of **Set1** contains a property that uses a forbidden unit (**Test1** returns true for at least
one ement of **Set1**)

#### Pre-Qualified

If a css could not have parsed, and so not tested for any reason AND no property of css rules of **Set1** uses a
forbidden unit (**Test1** returns false for all the elements of **Set1**)


## Files

- [TestCases files for rule 10.4.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule100401/)
- [Unit test file for rule 10.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule100401Test.java)
- [Class file for rule 10.4.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule100401.java)


---
title: "Rule 12.2.1"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test is of site scope or group of pages scope. When requesting a page audit, the result is automatically set to
"Not Applicable". No check is done instead.

## Business description

### Criterion

[12.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-12-2)

### Test

[12.2.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-12-2-1)

### Description

> Dans chaque
> [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages),
> chaque page disposant d’un
> [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation)
> vérifie-t-elle ces conditions (hors cas particuliers) ?
>
> * Le
>     [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation)
>     est toujours à la même place dans la présentation.
> * Le
>     [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation)
>     se présente toujours dans le même ordre relatif dans le code source.

#### Particular cases (criterion 12.2)

> Il existe une gestion de cas particulier lorsque :
>
> * Les pages d’un
>     [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages)
>     sont le résultat ou une partie d’un processus (un processus de paiement ou de prise de commande, par exemple) ;
> * La page est la page d’accueil ;
> * le site web est constitué d’une seule page.
>
> Dans ces situations, le critère est non applicable.

### Level

AA


## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Applicable

Page audit

#### Not Tested

Group of pages audit or site audit


## Files

* [TestCases files for rule 12.2.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule120201/)
* [Unit test file for rule 12.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule120201Test.java)
* [Class file for rule 12.2.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule120201.java)



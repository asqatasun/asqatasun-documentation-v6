---
title: "Theme 3: Colours"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 3.1

> Dans chaque page web, l’
> [information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#information-donnee-par-la-couleur)
> ne doit pas être donnée uniquement par la couleur. Cette règle est-elle respectée ?

* [Rule 3.1.1]({{< relref "./Rule-3-1-1" >}})
* [Rule 3.1.2]({{< relref "./Rule-3-1-2" >}})
* [Rule 3.1.3]({{< relref "./Rule-3-1-3" >}})
* [Rule 3.1.4]({{< relref "./Rule-3-1-4" >}})
* [Rule 3.1.5]({{< relref "./Rule-3-1-5" >}})
* [Rule 3.1.6]({{< relref "./Rule-3-1-6" >}})

## Criterion 3.2

> Dans chaque page web, le
> [contraste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contraste) entre la
> couleur du texte et la couleur de son arrière-plan est-il suffisamment élevé (hors cas particuliers) ?

* [Rule 3.2.1]({{< relref "./Rule-3-2-1" >}})
* [Rule 3.2.2]({{< relref "./Rule-3-2-2" >}})
* [Rule 3.2.3]({{< relref "./Rule-3-2-3" >}})
* [Rule 3.2.4]({{< relref "./Rule-3-2-4" >}})
* [Rule 3.2.5]({{< relref "./Rule-3-2-5" >}})

## Criterion 3.3

> Dans chaque page web, les couleurs utilisées dans les
> [composants d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface)
> ou les éléments graphiques porteurs d’informations sont-elles suffisamment contrastées (hors cas particuliers) ?

* [Rule 3.3.1]({{< relref "./Rule-3-3-1" >}})
* [Rule 3.3.2]({{< relref "./Rule-3-3-2" >}})
* [Rule 3.3.3]({{< relref "./Rule-3-3-3" >}})
* [Rule 3.3.4]({{< relref "./Rule-3-3-4" >}})


---
title: "RGAA v4.0.0"
date: 2020-11-12T17:14:53+02:00
weight: 40
---

## RGAA v4 business rules

* [Theme 1 - Images]({{< relref "./01.Images/" >}})
* [Theme 2 - Frames]({{< relref "./02.Frames/" >}})
* [Theme 3 - Colors]({{< relref "./03.Colours" >}})
* [Theme 4 - Multimedia]({{< relref "./04.Multimedia" >}})
* [Theme 5 - Tables]({{< relref "./05.Tables" >}})
* [Theme 6 - Links]({{< relref "./06.Links" >}})
* [Theme 7 - Script]({{< relref "./07.Scripts" >}})
* [Theme 8 - Mandatory elements]({{< relref "./08.Mandatory_elements" >}})
* [Theme 9 - Structure of information]({{< relref "./09.Structure_of_information" >}})
* [Theme 10 - Presentation of information]({{< relref "./10.Presentation_of_information" >}})
* [Theme 11 - Forms]({{< relref "./11.Forms" >}})
* [Theme 12 - Navigation]({{< relref "./12.Navigation" >}})
* [Theme 13 - Consultation]({{< relref "./13.Consultation" >}})

## RGAA v4 in Asqatasun

As of Asqatasun v5.0.0, the implementation of RGAA v4 covers:

* **117 rules**, over 257 tests, which represents **45% of RGAAv4**
* Please have a look at
    [Type of Result by Rule]({{< relref Type-of-result-by-rule >}}) for detailed figures

## RGAA v4 figures

* 13 themes
* 106 criteria
* 257 tests
    * 200 test of level A
    * 57 tests of level AA

## Links and references

RGAA v4.0.0 was published on 2019-10

* [RGAA 4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/)
    * [RGAA v4 - Criteria and tests](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/)
    * [RGAA v4 - Glossary](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/)
    * [Revision notes v3.2017 / v4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/notes-revision/)
* [Github repository RGAA v4](https://github.com/DISIC/RGAA)
* [RGAA v4 - Méthodologie de test](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methodologie-test/#contenu)
    * Useful as it describes the manual algorithm to actually assess each test
    * [source code Github](https://github.com/DISIC/RGAA-tests/blob/master/methodologie.md)
      (different from repository of RGAAv4)

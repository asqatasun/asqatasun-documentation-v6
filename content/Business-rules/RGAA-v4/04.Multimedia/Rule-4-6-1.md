---
title: "Rule 4.6.1"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test consists in detecting all the links allowing to download a video file and all the tags allowing to display a
video or an animated cartoon.

## Business description

### Criterion

[4.6](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-4-6)

### Test

[4.6.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-4-6-1)

### Description

> Pour chaque
> [média temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-temporel-type-son-video-et-synchronise)
> pré-enregistré seulement vidéo ayant une
> [audiodescription](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#audiodescription-synchronisee-media-temporel)
> synchronisée, celle-ci est-elle pertinente ?

### Level

AA


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

- All links to download a video file
- AND all the following tags:
    - `<video>`
    - `<object>`
    - `<embed>`
    - `<svg>`
    - `<canvas>`

css selector :

```css
video[src],
video:has(source[src]),
object[data],
embed[src],
svg,
canvas,
a[href~=(?i)\.(mp4|avi|wmv|mov|Xvid|mkv|mka|mks|FLV|rmvb|MPA|WMA|MP2|M2P|DIF|DV|VOB|VRO|rmvb|vivo|bik|ASF|ifo|mts|mxf|nds|rv|web|wlmp|wmp|ogv)]
```

### Process

#### Test1

Test whether **Set1** is not empty. If yes, raise a MessageA.

##### MessageA : Check manually the elements of the scope

- code: ManualCheckOnElements
- status: Pre-qualified
- parameter: snippet
- present in source: yes

### Analysis

#### Not Applicable

- The page has no `<video>`, `<object>`, `<embed>`, `<svg>` or `<canvas>` tag.
- The page has no link to download a video file.

#### Pre-qualified

In all other cases


## Files

- [TestCases files for rule 4.6.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule040601/)
- [Unit test file for rule 4.6.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule040601Test.java)
- [Class file for rule 4.6.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule040601.java)



---
title: "Rule 4.12.2"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test consists in detecting all the tags for no time-based media.

## Business description

### Criterion

[4.12](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-4-12)

### Test

[4.12.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-4-12-2)

### Description

> Pour chaque
> [média non temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-non-temporel),
> chaque fonctionnalité vérifie-t-elle une de ces conditions ?
>
> * La fonctionnalité est
>     [activable par le clavier et la souris](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage).
> * Une fonctionnalité
>     [activable par le clavier et tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage)
>     permettant de réaliser la même action est présente dans la page.

### Level

A


## Technical description

### Scope

Page

### Decision level

Semi-Decidable


## Algorithm

### Selection

#### Set1

* All the following tags:
    * `<svg>`
    * `<canvas>`
    * `<object>`
    * `<embed>`

css selector :

```css
svg,
canvas,
object[data],
embed[src]
```

### Process

#### Test1

Test whether **Set1** is not empty. If yes, raise a MessageA.

##### MessageA : Check manually the elements of the scope

* code: ManualCheckOnElements
* status: Pre-qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

The page has no `<svg>`, `<canvas>`, `<object>` or `<embed>` tag.

#### Pre-qualified

In all other cases


## Files

* [TestCases files for rule 4.12.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule041202/)
* [Unit test file for rule 4.12.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule041202Test.java)
* [Class file for rule 4.12.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule041202.java)



---
linkTitle: "RGAA in Asqatasun"
title: "RGAA in Asqatasun"
date: 2022-01-14T18:25:15+01:00
weight: 10
---

## RGAA Structure

RGAA is structured in the following way:

* theme
    * criteria
        * test

A *theme* is compound of several *criteria*, which in turn is compound of one or more *tests*.

## Vocabulary used in Asqatasun

A *test* is the object from the accessibility business, child of a criteria, and describing from a technical point of
view what to verify and how to do it.

Once a test is implemented in Asqatasun, we call it a *rule*. So a *rule* refers to the (Java) code that reflects the
intent of the accessibility business.

Having two different words is important as it allows us to distinguish those two different objects.

---
title: "Administrator manual"
date: 2023-12-22T17:33:40+02:00
weight: 20
---

## How to access to the BackOffice

Once logged in, you may click on the "gears" icon in the upper right corner.
(If you don't see it, you may not be an admin.)

![](./ASQATASUN_v4.0_access_to_backoffice.png)

Once there, you may:

* create / update / delete users
* create / update / delete projects for each user

## Add a new project to a user

* A - Access to the BackOffice : click on the "gears" icon in the upper right corner.
* B - Click on the "folder" icon of one user to manage his contracts / projects.
* C - Click on the "Add a contract" button.

![](backoffice_contract_manage.png)

![](backoffice_contract_list.png)

![](backoffice_contract_add.png)

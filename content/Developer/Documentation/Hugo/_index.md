---
title: "Hugo"
date: 2023-12-17T11:45:42+02:00
weight: 10
---

## Hugo links

* [Hugo documentation](https://gohugo.io/documentation/)
* [Hugo forum](https://discourse.gohugo.io/) (really useful)

## How to install Hugo on Debian-based linux distributions

* Download the [`.deb` package from Hugo releases](https://github.com/gohugoio/hugo/releases) on Github
* run `sudo dpkg -i hugo_XX.YY.ZZ_Linux-64bit.deb`

## Hugo basics

* All content is written in Markdown, and placed in `content/` directory.
* Configuration of the website is in `config/` directory.
* Once build, the generated website is placed in `static/` directory (important for Gitlab Pages).
* Themes may be used, they are placed in `themes/` directory. Please note they are inserted as git submodules.

## Hugo integration within IntelliJ

* Plugin [Hugo Integration](https://plugins.jetbrains.com/plugin/13215-hugo-integration)
* Or directly within IntelliJ: `File` > `Settings` > `Plugins`, then search `Hugo`

## Run Hugo locally

From the repository's root:

```shell script
hugo server --buildDrafts
```

Then browse [http://localhost:1313/](http://localhost:1313/)

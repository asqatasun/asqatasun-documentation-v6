---
title: "Link checking"
date: 2023-12-17T20:13:55+02:00
weight: 20
---

## Check links locally: with Lychee

Resources: <https://github.com/lycheeverse/lychee>

### Verify links between our own pages

Link checking is made on the filesystem. **Really fast!** (<10 seconds)

```shell
docker run --init -it --rm -w /input -v $(pwd):/input lycheeverse/lychee --format detailed --exclude-all-private --offline content/
```

### Verify links, also to external websites

Quite fast: a few minutes

```shell
docker run --init -it --rm -w /input -v $(pwd):/input lycheeverse/lychee --format detailed --exclude-all-private content/
```

## Check links on PRODUCTION

Verify only `/v6/` directory:

```shell
docker run --init -it --rm -w /input -v $(pwd):/input lycheeverse/lychee --format detailed --exclude-all-private https://doc.asqatasun.org/v6/
```

## Check links with Gitlab-CI

TODO implement Lychee in gitlab-ci

---
linkTitle: "Documentation"
title: "Asqatasun Documentation"
date: 2023-12-17T09:33:01+02:00
weight: 80
---

## Organisation of doc.asqatasun.org

Documentation website is a static site made with Hugo.

## References

* Documentation webite is [https://doc.asqatasun.org](https://doc.asqatasun.org)
* Repositories for this website are:
    * [Doc for Asqatasun v4](https://gitlab.com/asqatasun/documentation-v4)
    * [Doc for Asqatasun v5](https://gitlab.com/asqatasun/documentation)
    * [Doc for Asqatasun v6](https://gitlab.com/asqatasun/asqatasun-documentation-v6)
    * [Home page](https://gitlab.com/asqatasun-websites/doc.asqatasun.org)


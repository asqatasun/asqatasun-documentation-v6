---
title: "Hugo theme"
date: 2024-10-17T11:45:42+02:00
weight: 11
---

## Hugo theme

This website uses the theme Hextra:

- [Hextra on Github](https://github.com/imfing/hextra/)
- [Hextra website](https://imfing.github.io/hextra/)
- [Hextra documentation](https://imfing.github.io/hextra/docs/)

## Update theme

To upgrade theme from version `x.y.z` to version `a.b.c`, do:

```shell
hugo mod get -u github.com/imfing/hextra@va.b.c
hugo mod tidy
# Optional: verify dependencies
hugo mod graph
```

**Notes**:

- do NOT modify the content of `go.mod` file, this is the job of `hugo mod get`
- don't forget to run `hugo mod tidy`, as it verifies the suitable version of Go and cleans up `go.mod`
- we need to have `go.mod` in git to ensure the same version across developers

## Backport our own changesets

To fix accessibility issues (and while contributions are reported upstream), we had to modify files from the theme. See
Hugo documentation (especially [this post in the forum](https://discourse.gohugo.io/t/overriding-theme-scss/29379/2))
for how this is made.

So modifications we made should be "rebased" each time we upgrade the theme. Here is te list of modified files:

- `layouts/partials/navbar.html`

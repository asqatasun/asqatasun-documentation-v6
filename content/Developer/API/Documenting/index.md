---
linkTitle: "Documenting"
title: "Documenting the API"
date: 2022-02-24T16:47:31+01:00
weight: 10
---

Asqatasun API is [OpenAPI 3](https://www.openapis.org/) compliant.

As a contributor to Asqatasun, you may need the following information.

The [code of the API](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/server) is documented with annotations,
that allows to easily have a comprehensive code base and the user interface provided by Swagger with all details
coming from the annotations.

When coding on the API, you may want to know the grammar or vocabulary describing the annotations. Here are
the suitable resources:

* <https://github.com/OAI/OpenAPI-Specification>
* <https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.3.md>
* <https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md>
* <https://spec.openapis.org/oas/v3.1.0#example-object>

Finally, the library used to bridge Spring Boot on one hand and OpenAPI 3 on the other hand is
[SpringDoc](https://springdoc.org/). You may find interesting the [Springdoc Demos](https://springdoc.org/#demos),
especially the *Demo Spring Boot 2 Web MVC with OpenAPI 3*, which [source code is downloadable](https://github.com/springdoc/springdoc-openapi-demos.git)

---
title: "Producing a Release"
date: 2024-02-20T10:34:32+01:00
weight: 30
---

Here are described the steps to produce a release

## 0. Pre-requisites

* [ ] CI is green
* [ ] A human being has applied [E2E testing for Asqatasun **server**](../Test/E2E-testing-server)
* [ ] A human being has applied [E2E testing for Asqatasun **webapp**](../Test/E2E-testing-webapp)
* [ ] [Documentation repos](https://gitlab.com/asqatasun/asqatasun-documentation-v6) is up-to-date, especially install
  doc and upgrade doc.
* [ ] [List of results per test](http://localhost:1313/Business-rules/RGAA-v4/Type-of-result-by-rule/) (passed, failed,
  NMI, NA, Not Tested) is up-to-date

## 1. Assumptions

* `RELEASE_VERSION` is the version string we're about to release. It must be SemVer compatible. Example:
  `6.0.0-alpha.1`
* `DEV_VERSION` is the version string of the code base:
    * *after* the release is created,
    * and for the immediate next release.
    * (It is not considered as a version in the sense of SemVer.)
    * Example: `6.0.0-SNAPSHOT`

```shell script
RELEASE_VERSION="6.0.0-alpha.1"
DEV_VERSION="6.0.0-SNAPSHOT"
```

## 2. Create a merge request to update content

- Update `CHANGELOG.md`
- Update `README.md`

Then update `contributors.txt`, from the root of the repos:

```shell script
engine/asqatasun-resources/src/main/resources/release/update_contributors.sh --to-version ${RELEASE_VERSION}
```

**Note**: the `update_contributors.sh` script relies on a `.mailmap` file, that groups various names / emails of
a given contributor into a unique one (couple name/email). As the `.mailmap` file contains a lot of personal emails
we chose deliberately *not* to place it in the repository.

Merge the merge request.

## 3. Launch release creation

* At the end of the pipeline on main branch (`main` or `master`), click on the **Maven_release** job. (Take care to
  click on the text, not on the "play" icon, you would miss the ability to define the values.)
  ![](job_maven_release_kraken.png)
* Type in the following keys with their values
    * `RELEASE_VERSION` (example value: `6.0.0-alpha.1`)
    * `NEXT_DEVELOPMENT_VERSION` (example value: `6.0.0-SNAPSHOT`)
* Then click on *Run job*.

This job will:

1. update the `pom.xml` files with `RELEASE_VERSION`,
2. create a git tag named `RELEASE_VERSION`,
3. create a gitlab-release named `RELEASE_VERSION`,
4. upload docker images to [Docker Hub, Asqatasun namespace](https://hub.docker.com/u/asqatasun)
5. update the `pom.xml` files with `NEXT_DEVELOPMENT_VERSION`.

## 4. Add details to the Gitlab release

Complete the gitlab-release details with:

- copy/pasting the changelog,
- defining the associated milestone.

## 6. Communicate

* Create a message on the forum. Do not forget to include screenshots.
  Example: [Asqatasun 6.0.0-alpha.1 is out! Huge maintenance work + PostgreSQL + contracts in API](https://forum.asqatasun.org/t/asqatasun-6-0-0-alpha-1-is-out-huge-maintenance-work-postgresql-contracts-in-api/925)
* Publish announce on Masto + Element

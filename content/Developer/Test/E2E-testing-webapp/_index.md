---
title: "E2E Testing webapp"
date: 2024-01-01T19:57:36+02:00
weight: 10
---

End-to-end testing for Asqatasun web application running locally, *on the developer desktop*.

## Assumptions

- Asqatasun is running locally on port 8080 <http://127.0.0.1:8080>

## Running E2E testing on webapp

```sh
cd testing-tools/E2E-testing-webapp
mvn test \
    -Dadmin.user=admin@asqatasun.org \
    -Dadmin.password=myAsqaPassword \
    -Dhost.location=http://localhost:8080/ \
    -Dfirefox.path=/opt/firefox/firefox \
    -Dgecko.path=/opt/geckodriver
```

## Running one given functional test

Say you just want to debug the
["login/logout" test](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/testing-tools/E2E-testing-webapp/src/test/java/org/asqatasun/tgol/test/scenario/LoginLogoutScenarioTest.java)
, simply add the argument `-Dtest=` with the class name as value.

```sh
cd testing-tools/E2E-testing-webapp
mvn test \
    -Dadmin.user=admin@asqatasun.org \
    -Dadmin.password=myAsqaPassword \
    -Dhost.location=http://localhost:8080/ \
    -Dfirefox.path=/opt/firefox/firefox \
    -Dgecko.path=/opt/geckodriver \
    \
    -Dtest=LoginLogoutScenarioTest
```


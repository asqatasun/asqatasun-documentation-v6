---
title: "E2E Testing server"
date: 2024-10-26T19:57:36+02:00
weight: 10
---

## Pre-requisites

- Hurl v5+, see [Hurl installation](https://hurl.dev/docs/installation.html)

## How to run

```shell
cd testing-tools/E2E-testing-server/

# Mandatory for site-audits
export HURL_now=$(date '+%Y-%m-%dT%H:%M:%S')
export HURL_later=$(date '+%Y-%m-%dT%H:%M:%S' -d"+1year")

# Global tests
hurl \
  --variables-file hurl-options.env \
  --user admin@asqatasun.org:myAsqaPassword \
  --report-html ./Report/ \
  --test Testsuite/*

# Test concurrent accesses  
hurl \
  --variables-file hurl-options.env \
  --user admin@asqatasun.org:myAsqaPassword \
  --test Testsuite/04*
```

For debugging purpose, you may add the `--verbose` parameter.

## Changing URL and port of server to be tested

Those details are stored in the `hurl-options.env` file. Here is an example:

```dotenv
proto=http
host=localhost
port=8080
path=/api/v0
```

## Resources

- <https://hurl.dev>
- <https://about.gitlab.com/blog/2022/12/14/how-to-continously-test-web-apps-apis-with-hurl-and-gitlab-ci-cd/>


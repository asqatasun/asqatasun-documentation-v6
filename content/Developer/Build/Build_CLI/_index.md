---
linkTitle: "Build CLI"
title: "Build with Command Line Interface"
date: 2023-10-11T13:35:09+02:00
weight: 12
---

(All this information is based
on [what the CI does](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/.gitlab-ci.yml))

{{% callout type="info" %}}
We assume the current working directory is where Asqatasun source code is.
For instance: `/home/asqatasun/Asqatasun/`
{{% /callout %}}

## Build

```shell
MAVEN_CLI_OPTS="--batch-mode"
MAVEN_OPTS="-Dmaven.repo.local=.m2/repository"
./mvnw $MAVEN_CLI_OPTS validate
./mvnw $MAVEN_CLI_OPTS clean
./mvnw $MAVEN_CLI_OPTS install -Dmaven.test.skip=true
./mvnw $MAVEN_CLI_OPTS test
mkdir -p \
  ~/var/log/asqatasun/webapp/ \
  ~/var/log/asqatasun/server/
```

You may build and skip the dependency-check task (which looks for known CVEs and takes some time) this way:

```shell
./mvnw $MAVEN_CLI_OPTS install -Dmaven.test.skip=true -Ddependency-check.skip=true
```

## Run webapp

Launch webapp (MariaDB):

```shell
ASQATASUN_RELEASE="6.0.0-SNAPSHOT"
DB_PORT="3307"
DB_HOST="localhost"
DB_DRIVER="mariadb"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="${HOME}/var/log/asqatasun/webapp"
WAR_FILE="${HOME}/Asqatasun/web-app/asqatasun-web-app/target/asqatasun-web-app-${ASQATASUN_RELEASE}.war"

/usr/bin/java \
    -Dapp.engine.loader.selenium.hubUrl=http://localhost:4444/ \
    -Dapp.logging.path="${APP_LOG_DIR}" \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${WAR_FILE}"
```

Launch webapp (PostgreSQL):

```shell
ASQATASUN_RELEASE="6.0.0-SNAPSHOT"
DB_PORT="5432"
DB_HOST="localhost"
DB_DRIVER="postgresql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="${HOME}/var/log/asqatasun/webapp"
WAR_FILE="${HOME}/Asqatasun/web-app/asqatasun-web-app/target/asqatasun-web-app-${ASQATASUN_RELEASE}.war"

/usr/bin/java \
    -Dapp.engine.loader.selenium.hubUrl=http://localhost:4444/ \
    -Dapp.logging.path="${APP_LOG_DIR}" \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${WAR_FILE}"
```

## Use webapp

See [Asqatasun usage](../../../User/Asqatasun-Usage/).

## Run server

With PostgreSQL:

```shell
ASQATASUN_RELEASE="6.0.0-SNAPSHOT"
DB_PORT="5432"
DB_HOST="localhost"
DB_DRIVER="postgresql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="${HOME}/var/log/asqatasun/server"
JAR_FILE="${HOME}/Asqatasun/server/asqatasun-server/target/asqatasun-server-${ASQATASUN_RELEASE}.jar"

/usr/bin/java \
    -Dapp.engine.loader.selenium.hubUrl=http://localhost:4444/ \
    -Dapp.logging.path="${APP_LOG_DIR}" \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${JAR_FILE}"
```


With MariaDB:

```shell
ASQATASUN_RELEASE="6.0.0-SNAPSHOT"
DB_PORT="3307"
DB_HOST="localhost"
DB_DRIVER="mariadb"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="${HOME}/var/log/asqatasun/server"
JAR_FILE="${HOME}/Asqatasun/server/asqatasun-server/target/asqatasun-server-${ASQATASUN_RELEASE}.jar"

/usr/bin/java \
    -Dapp.engine.loader.selenium.hubUrl=http://localhost:4444/ \
    -Dapp.logging.path="${APP_LOG_DIR}" \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${JAR_FILE}"
```

## Use server

See [Asqatasun usage](../../../User/Asqatasun-Usage/#use-server-with-swagger-web-interface).

## Stop service (webapp or server)

Just type `CTRL-C` on the console.

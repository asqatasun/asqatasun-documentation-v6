---
linkTitle: "Debug"
title: "Debugging Asqatasun"
date: 2022-02-17T15:42:51+01:00
weight: 20
---

## Add more log

You may add or customize logs in the two main projects (webapp or REST API server). In both cases it is in
the `logback-spring.xml` file:

* for the
  webapp: [`web-app/asqatasun-web-app/src/main/resources/logback-spring.xml`](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/web-app/asqatasun-web-app/src/main/resources/logback-spring.xml)
* for the REST API
  Server: [`server/asqatasun-server/src/main/resources/logback-spring.xml`](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/server/asqatasun-server/src/main/resources/logback-spring.xml)

For instance, to display all the DEBUG information related to the Firefox managed by Selenium, you may add:

```xml

<configuration>
    ...
    <logger name="jp.vmi.selenium.selenese" level="DEBUG"/>
    ...
</configuration>
```

## Display the Firefox launched by Asqatasun

You may display the Firefox window that actually loads the audited webpage when you
[run Asqatasun in your IntelliJ](../Pre-requisites). This is especially useful when you try to understand why
[a scenario](../../../User/Kinds-of-audit/Scenario-audit) fails. To do so, you can add in the *VM options*:

```shell script
-Dapp.engine.loader.selenium.headless=false
```

You may also pass the same option as *Program arguments*:

```shell script
--app.engine.loader.selenium.headless=false
```

(Note that `-D` is replaced by `--`)



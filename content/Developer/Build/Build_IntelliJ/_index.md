---
linkTitle: "Build IntelliJ"
title: "Build with IntelliJ IDEA"
date: 2023-10-26T18:41:14+02:00
weight: 14
---

## Assumptions

* You have fulfilled all the [dev pre-requisites](../Pre-requisites/).
* Source code is located in `~/Asqatasun` (i.e. you made your `git clone` from your `$HOME`).

## Import project

In IntelliJ, choose `File` > `New` > `Project from existing sources...`.

Then you **need** to *import project from external model*, and choose Maven.

![](IntelliJ_Import_Project_001.png)

If you don't do that, you won't be able to build/run Asqatasun.

## Configure JDK

(That might sound stupid as you do already have a JDK, but we want to ensure you have the correct one, with the correct
configuration :) )

* Go to `File` > `Project Structure` or do `CTRL`+`ALT`+`SHIFT`+`S`
* Under *Platform Settings*, click *SDK*
* Click on the plus (`+`) icon, select *Download SDK*
* Choose Version `17`, Vendor `Eclipse Temurin (AdoptOpenJDK HotSpot)` and validate
* Ensure the select JDK is `temurin-17`.

![](IntelliJ_SDK.png)

## (Re)Load Maven project

Reloading "all Maven projects" is needed to automatically discover and configure SpringBoot projects. To do so:
press `SHIFT`+`SHIFT`, type in `reload all maven projects` and validate.

After the Maven projects are reloaded and indexed, IntelliJ offers the ability to use *Services* to manage Spring Boot
run configuration: accept this.

![](IntelliJ_Springboot_run_configurations_with_Services.png)

A good way to verify the projects are correctly reloaded is to go to `File` > `Project Structure` or
`CTRL`+`ALT`+`SHIFT`+`S`. In the *Problems* section, you should have **zero** entry.

![](IntelliJ_Project_structure_problems.png)

## Configure Maven Wrapper

You must configure the project to leverage Maven Wrapper. To do so:

* get to `File` > `Settings...` (`CTRL-ALT-S`),
* navigate to `Build, Execution, Deployment` > `Build Tools` > `Maven`,
* set parameter `Maven home path` to value `Use Maven wrapper`.

Here is a screenshot from IntelliJ 2021.3.2:

![](IntelliJ_maven_wrapper.png)

## Configure character encoding

* Get to `File` > `Settings` (`CTRL`+`ALT`+`S`)
* Go into *Editor* > *File encodings*
* For the key *Default encoding for properties file:*, set value **UTF-8**

## Edit Run/Debug configurations > Asqatasun Webapp

From projet root, do:

```shell
cd web-app/asqatasun-web-app/src/main/resources
ln -s ../webapp/public public
```

(More information on this in [issue #639](https://gitlab.com/asqatasun/Asqatasun/-/issues/639))

Get to `Run` > `Edit configurations...`. Under *Spring Boot*, click on `AsqatasunWebappApplication`.
Add the following values:

* VM Options: `-Dwebdriver.gecko.driver=/opt/geckodriver -Djdbc.url="jdbc:mysql://localhost:3307/asqatasun"`
* Working directory: `$MODULE_WORKING_DIR$`

Here is a screenshot from IntelliJ 2021.3.2:

![](IntelliJ_Run_Edit_configurations_AsqatasunWebapp.png)

Note: in IntelliJ 2021.3.3, this dialog has slightly changed; to get *Working directory*, you should click on
*Modify options*, then tick *Working directory* to make it appear in the main dialog.

## Edit Run/Debug configurations > Asqatasun Server

Get to `Run` > `Edit configurations...`. Under *Spring Boot*, click on `AsqatasunServer`.
Add the following values:

* VM Options: `-Dwebdriver.gecko.driver=/opt/geckodriver -Djdbc.url="jdbc:mysql://localhost:3307/asqatasun"`

![](IntelliJ_Run_Edit_configurations_AsqatasunServer.png)

## Configuration of Spring Boot projects

Source code holds many projects but two of them are real entrypoints:

* **Asqatasun Webapp**: the historical Asqatasun with the engine and the web-application.
* **Asqatasun Server**: added since v5.0.0, it holds the engine and the REST API.

In a Spring Boot project, all the configuration lies in `application.yml`. Here are the respective paths:

* Webapp: `web-app/asqatasun-web-app/src/main/resources/application.yml`
* Server: `server/asqatasun-server/src/main/resources/application.yml`

You can override configuration with arguments passed as *VM options*. Get inspired by what we do in
[Build CLI](../Build_CLI/), or how we do in
[production installation](../../../Operator/Installation/Regular/)
using systemd units + a local configuration file.

Globally, configuration can be overridden thanks to
[Spring Boot Externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.external-config)
.

## Execute

* `Run`> `Run..` and choose `AsqatasunWebappApplication` or `AsqatasunServer`
* Then browse <http://localhost:8080/> and connect with credentials:
    - login: `admin@asqatasun.org`
    - password: `myAsqaPassword`

{{% callout type="info" %}}
For Asqatasun Server, in the Swagger UI, you must change the value of `contractId` from `0` to `1`
or your request will end in a 500 error.
{{% /callout %}}



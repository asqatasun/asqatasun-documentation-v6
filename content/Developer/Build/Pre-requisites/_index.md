---
linkTitle: "Pre-requisites"
title: "Pre-requisites for development"
date: 2024-10-06T12:08:54+02:00
weight: 10
---

We describe how to create a developer environment to build Asqatasun locally
on an **Ubuntu 22.04** / **Linux Mint 21**

## Maven

You don't have to download Maven, as it is embedded in the project thanks to Maven Wrapper. To use Maven, get to the
root of this repository, and use `./mvnw` (instead of `mvn`).

## JDK 17

```bash
sudo apt install openjdk-17-jre
sudo update-alternatives --config java
java -version
```

### Define JAVA_HOME

Define `JAVA_HOME` as the path indicated by `update-java-alternatives -l` for the version of Java you use.

For instance, if you have:

```shell
% update-java-alternatives -l
java-1.11.0-openjdk-amd64      1111       /usr/lib/jvm/java-1.11.0-openjdk-amd64
java-1.17.0-openjdk-amd64      1711       /usr/lib/jvm/java-1.17.0-openjdk-amd64
java-1.8.0-openjdk-amd64       1081       /usr/lib/jvm/java-1.8.0-openjdk-amd64
```

... and you use Java 17, then you should define JAVA_HOME this way:

```shell
export JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64  # path indicated for Java 17 by "update-java-alternatives -l" 
```

To avoid defining it at each boot, you may add it to your profile. For ZSH users, it can be:

```shell
echo "export JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64" >> ~/.zshrc
```

## Firefox + Gecko Driver

### Selenium Grid (recommended)

The recommended way since Asqatasun v6 is to use a Selenium Grid docker image

```shell
docker run \
  --name selenium-ff \
  -p 4444:4444 \
  -p 7900:7900 \
  --shm-size="2g" \
  -d \
  selenium/standalone-firefox:122.0-geckodriver-0.34.0-grid-4.17.0-20240123
```

### Local installation of Firefox & Geckodriver (old fashion)

```shell script
FIREFOX_VERSION="102.8.0esr"
GECKODRIVER_VERSION="v0.32.2"
FIREFOX_URL_PREFIX="https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
GECKODRIVER_URL_PREFIX="https://github.com/mozilla/geckodriver/releases/download/"
FIREFOX_URL="${FIREFOX_URL_PREFIX}${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2"
GECKODRIVER_URL="${GECKODRIVER_URL_PREFIX}${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

cd /opt
sudo wget "${FIREFOX_URL}"
sudo wget "${GECKODRIVER_URL}"
sudo tar xf "firefox-${FIREFOX_VERSION}.tar.bz2"
sudo tar xf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
```

**Notes**:

* It is important to stick to the specified version of Firefox.
* Firefox version and GeckoDriver version are strongly tied:
  see [table on GeckoDriver documentation](https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html)

## RDBM

You may choose between MariaDB, PostgreSQL or HsqlDB. The later is recommended for testing or CI and is included in
Asqatasun.

### Postgresql 16

```shell script
docker run \
  --name postgres \
  -e POSTGRES_USER=asqatasunDatabaseUserLogin \
  -e POSTGRES_PASSWORD=asqatasunDatabaseUserP4ssword \
  -e POSTGRES_DB=asqatasun \
  -p 5432:5432 \
  -d \
  postgres:16
```

Notes:

* The values `asqatasunDatabaseUserLogin`, `asqatasunDatabaseUserP4ssword` and `asqatasun` are the default ones.
  They are defined in
  [`application.yml` (webapp)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/web-app/asqatasun-web-app/src/main/resources/application.yml#L4)
  and
  [`application.yml` (server)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/server/asqatasun-server/src/main/resources/application.yml#L4).

### MariaDB 10.6

```shell script
docker run \
  --name mysql \
  -e MYSQL_ROOT_PASSWORD=my-secret-pw \
  -e MYSQL_USER=asqatasunDatabaseUserLogin \
  -e MYSQL_PASSWORD=asqatasunDatabaseUserP4ssword \
  -e MYSQL_DATABASE=asqatasun \
  -p 3307:3306 \
  -d \
  mariadb:10.6
```

Notes:

* In this snippet, we choose to launch Mysql on port `3307` in case another Mysql is already running on `3306`
* The values `asqatasunDatabaseUserLogin`, `asqatasunDatabaseUserP4ssword` and `asqatasun` are the default ones
  and defined in [
  `application.yml` (webapp)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/web-app/asqatasun-web-app/src/main/resources/application.yml#L4)
  and [
  `application.yml` (server)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/server/asqatasun-server/src/main/resources/application.yml#L4)

## Get Asqatasun source code

```shell script
git clone https://github.com/Asqatasun/Asqatasun.git
cd Asqatasun
```

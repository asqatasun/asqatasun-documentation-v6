---
linkTitle: "Data Model"
title: "Data Model"
date: 2021-09-03T20:31:54+02:00
weight: 20
---

## Data model

* Download  [Asqatasun data Model: Mysql WorkBench format, 90kb](./Asqatasun_v5_data_model.mwb)
* Download  [Asqatasun data Model: SVG format, 1.2Mb](./Asqatasun_data_model.svg)
* Download  [Asqatasun data Model: PNG format, 790kb](./Asqatasun_data_model.png)
* Download  [Asqatasun data Model: PDF format, 93kb](./Asqatasun_data_model.pdf)

![Asqatasun data model](./Asqatasun_data_model.png)

## Main parts of the model

`audit` is the main entity, at the center of the model.

The *Accessibility business* part (lower right in the model) gathers tests, criteria, themes and references (like RGAA).

The `web_resource` entity is the object that is audited, whether a single page or whole site. Its companion
entity `content` holds the true content, i.e. the HTML, CSS, JS blocs. These entities are grouped in the
*Content part* (left part of the model).

At the lower left part of the model resides the *Results* part. Its main entity is *process_result*, which holds
the actual detailed result of a test for a given audit.

The *Parameters* part (center top in the model) groups all the parameters an audit can have (which referential, which
level, etc).

At the top center of the model, we find the *tags* part. Tags, in Asqatasun v5.0, are used only by the API. They provide
an easy way to grab a group of audits.

The *Contract* part (top right in the model) defines the projects a user can have.

## Detailed description: contract

`functionality` holds the type of audit a contract can do: page, site, file upload, scenario or manual.
`contract_functionality` is the association table between a given contract and the type of audits it is allowed to run.

`referential` lists the available referentials (RGAA3, RGAA4, SEO...).
`contract_referential` is the association table between a given contract and the referential(s) against which it is
allowed to run audits.


---
linkTitle: "Dependencies"
title: "Dependency management"
weight: 10
---

## Identify security issues in dependencies

### OWASP Dependency check

We use [OWASP Dependency check](https://owasp.org/www-project-dependency-check/) Maven plugin that lists issues with
Maven dependencies.

Usage:

```shell
./mvnw  org.owasp:dependency-check-maven:check -DcveValidForHours=12
```

* Provide the full namespace `org.owasp:` otherwise on some projects the goal may not be found.
* `cveValidForHours` defines in hours when to re-download all the CVE database (which is a long operation). Default
  value is 4 hours, we increase it to 12.

The Maven plugin generates, *for each Maven project*, a file `target/dependency-check-report.html` presenting all the
security issues.

### Methodology to upgrade dependencies

* For a given Maven project (e.g. `web-app/asqatasun-web-app/`),
* from the `target/dependency-check-report.html` report,
* for a given dependency with security issue,
* follow these steps:

1. Search the considered dependency in the `pom.xml` of the project
2. If it is present:
    1. Upgrade it with the help of <https://mvnrepository.com/>
    2. Build with command line:
       `./mvnw $MAVEN_CLI_OPTS validate clean install -Dmaven.test.skip=true && ./mvnw $MAVEN_CLI_OPTS test`
    3. Run an audit on the Web-app project
    4. Run an audit on the Server project
3. If it is not present:
    * This is a transitive dependency, i.e. brought by another dependency.
    * That upper dependency should be treated first.
4. Repeat for each dependency

### Resources

* [Dependency check documentation](https://jeremylong.github.io/DependencyCheck/) (Important to read as many concepts
  are laid down.)
* [Dependency check Maven plugin: documentation](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/index.html)
* [Dependency check Maven plugin: goals and configuration](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/configuration.html)

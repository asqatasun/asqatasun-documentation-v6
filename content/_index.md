---
title: "Asqatasun v6 documentation"
date: 2023-10-08T17:34:36+02:00
cascade:
  type: docs
---

![Asqatasun]({{< baseurl >}}/images/Asqatasun_logo_RVB_crop.svg)

Asqatasun is an opensource website analyzer, used for web accessibility (a11y) and Search Engine Optimization (SEO).

## Demo

[https://app.asqatasun.org](https://app.asqatasun.org)

[![https://app.asqatasun.org]({{< baseurl >}}/images/Screenshot_2020-08-09_Asqatasun_home_kraken.png)](https://app.asqatasun.org)

## Features

**Web accessibility assessment** `#a11y` (RGAA, WCAG)

* scan a whole site for a11y issues (crawler included)
* scan a given page, and manually fulfill the audit to produce report
* scan offline file (e.g. template being created but not online yet)
* scan a user-workflow like site registration, form completion or e-commerce checkout with **Asqatasun scenarios**.

## Vision

1. Automate as much as we can and even more :)
2. Be 200% reliable (don't give erroneous result)
3. have technological fun

## Installation and documentation

Online documentation: [doc.asqatasun.org](https://doc.asqatasun.org/)

## Download

[Asqatasun releases](https://gitlab.com/asqatasun/Asqatasun/-/releases)

## Contact and discussions

* [Asqatasun forum](https://forum.asqatasun.org/)
* [Instant messaging on Element.io: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
* email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )

## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
2. Give us [feedback on the forum](https://forum.asqatasun.org)
3. [Fill in bug report](https://gitlab.com/Asqatasun/Asqatasun/issues)
4. [Contribute](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code


## License

[affero GPL v3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/LICENSE)

## Have Fun

Happy testing !

[Asqatasun Team](https://gitlab.com/Asqatasun/Asqatasun/blob/master/documentation/en/asqatasun-team.md)



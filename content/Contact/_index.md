---
title: "Contact"
date: 2023-11-14T14:34:36+02:00
weight: 90
---

{{< icon "user-group" >}} [Asqatasun Forum](http://forum.asqatasun.org/)

{{< icon "element.io" >}} Element.io: [`+asqatasun:matrix.org`](https://app.element.io/#/group/+asqatasun:matrix.org)

{{< icon "mail" >}} Email: `asqatasun AT asqatasun DOT org`

---
linkTitle: "Pre-Requisites"
title: "Pre-Requisites"
date: 2023-11-22T10:53:53+02:00
weight: 20
---

You should have already done these steps:

1. [Check Hardware provisioning](../Provisioning/)

## Supported OSes (yet)

* Ubuntu 22.04

## Required packages - Ubuntu 22.04

* Java JRE 17
* MariaDB 10.6
* libGTK 3
* libDbus
* haveged (if missing your server will take 10 minutes to start!
  See <https://gitlab.com/asqatasun/Asqatasun/-/issues/574>)

```shell script
apt-get --no-install-recommends install \
    openjdk-17-jre         \
    libgtk-3-0             \
    libdbus-glib-1-2       \
    haveged                \
    mariadb-server-10.6
```

## Database

Create a database and the associated user:

```shell script
unset HISTFILE    # Do not keep the following commands in history
DB_DATABASE='asqatasun'
DB_USER='asqatasunDatabaseUserLogin'
DB_PASSWORD='asqatasunDatabaseUserP4ssword'
DB_TOHOST='localhost'

mysql -u root -e "CREATE USER ${DB_USER}@${DB_TOHOST} IDENTIFIED BY \"${DB_PASSWORD}\";"
mysql -u root -e "CREATE DATABASE ${DB_DATABASE} CHARACTER SET utf8;"
mysql -u root -e "GRANT ALL PRIVILEGES ON ${DB_DATABASE}.* TO ${DB_USER}@${DB_TOHOST};"
mysql -u root -e "FLUSH PRIVILEGES;"
```

## Database timezone

To use a named-timezone like `Europe/Paris`, we need first to fulfill Mysql tables, as per
[MariaDB doc: Timezone installation](https://mariadb.com/kb/en/mariadb-tzinfo-to-sql/).

As `root`, populate tables of `mysql` database:

```shell
mariadb-tzinfo-to-sql /usr/share/zoneinfo | mysql -u root -p mysql
```

From Mysql (as root user), define your named timezone:

```mysql
SET GLOBAL time_zone = 'Europe/Paris';
SET time_zone = 'Europe/Paris';
```

You can verify with:

```mysql
SELECT @@GLOBAL.time_zone, @@SESSION.time_zone;
```

**Note:** this acts only for the current session, if you restart Mysql the timezone will be lost. To set this
configuration permanent, proceed to next paragraph.

## Database server configuration -- MariaDB

```shell script
CNF_FILE="/etc/mysql/mariadb.conf.d/90-asqatasun.cnf"
touch "${CNF_FILE}"
cat >"${CNF_FILE}" <<EOF
[client]
default-character-set=utf8mb4

[mysql]
default-character-set=utf8mb4

[mysqld]
default_time_zone=Europe/Paris
collation-server=utf8mb4_general_ci
init-connect='SET NAMES utf8mb4'
character-set-server=utf8mb4
max_allowed_packet=64M
innodb_file_per_table
innodb_log_file_size=1G
innodb_buffer_pool_size=4G
# CAUTION we must respect the ratio:
# innodb_buffer_pool_size = 4 x innodb_log_file_size
EOF
```

Notes:

* Replace `Europe/Paris` with the suitable timezone for you
* Naming the file `90-asqatasun.cnf` is important as it must be loaded **after** `50-mysqld_safe.cnf`.
* Do not forget to restart Mysql server: `systemctl restart mariadb.service`.

## Database server configuration -- Mysql

```shell script
cat >/etc/mysql/mysql.conf.d/z_asqatasun.cnf <<EOF
[client]
default-character-set=utf8mb4

[mysql]
default-character-set=utf8mb4

[mysqld]
default_time_zone=Europe/Paris
collation-server=utf8mb4_general_ci
init-connect='SET NAMES utf8mb4'
character-set-server=utf8mb4
max_allowed_packet=64M
innodb_file_per_table
innodb_log_file_size=1G
innodb_buffer_pool_size=4G
# CAUTION we must respect the ratio:
# innodb_buffer_pool_size = 4 x innodb_log_file_size
EOF
```

Notes:

* Replace `Europe/Paris` with the suitable timezone for you
* Naming the file `z_asqatasun.cnf` (beginning with a "z") is important as it must be loaded **after** `mysqld.cnf`.
* Do not forget to restart Mysql server: `systemctl restart mysql.service`.

## Firefox ESR

* Firefox ESR
* Gecko Driver

```shell script
FIREFOX_VERSION="102.8.0esr"
GECKODRIVER_VERSION="v0.32.2"
FIREFOX_URL_PREFIX="https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
GECKODRIVER_URL_PREFIX="https://github.com/mozilla/geckodriver/releases/download/"
FIREFOX_URL="${FIREFOX_URL_PREFIX}${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2"
GECKODRIVER_URL="${GECKODRIVER_URL_PREFIX}${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

cd /opt
sudo wget "${FIREFOX_URL}"
sudo wget "${GECKODRIVER_URL}"
sudo tar xf "firefox-${FIREFOX_VERSION}.tar.bz2"
sudo tar xf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
```

**Notes**:

* It is important to stick to the specified version of Firefox.
* Firefox version and GeckoDriver version are strongly tied:
  see [table on GeckoDriver documentation](https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html)

## Mail SMTP

Asqatasun works better with email (informing you when an audit is finished, or if gives an error). Here are the steps to
install locally an SMTP server. You can also use online services such as MailJet or Mandrill and configure it into
`/etc/asqatasun/asqatasun.org`

Install the following packages:

```shell script
sudo apt-get install postfix mailutils
sudo dpkg-reconfigure postfix
```

Once the configuration is displayed, options to take are :

* **Configuration type:** Satellite system
* **SMTP relay:** *leave it empty* (this is the trick, don't type anything here)

## Next step

Congratulations, you survived the manual pre-requisites stage :)

You can go to
[Installation](../Installation/).

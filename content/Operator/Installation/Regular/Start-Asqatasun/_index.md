---
linkTitle: "Start Asqatasun"
title: "Start Asqatasun services"
date: 2023-11-22T15:56:26+01:00
weight: 35
---

You should have already done these steps:

1. [Check Hardware provisioning](../Provisioning/)
2. [Check pre-requisites](../Pre-requisites/)
3. [Installation](../Installation/)

(Ever need help ? Go to
[Asqatasun Forum](https://forum.asqatasun.org).)

## Launch Asqatasun with systemd

Start Asqatasun:

```shell script
systemctl start asqatasun-webapp
systemctl start asqatasun-server
```

Get status of Asqatasun services:

```shell script
systemctl status asqatasun-webapp
systemctl status asqatasun-server
```

Browse Asqatasun logs:

```shell script
journalctl -u asqatasun-webapp.service
journalctl -u asqatasun-server.service
```

`-f` option continuously prints new entries as they are appended to the journal

Stop Asqatasun:

```shell script
systemctl stop asqatasun-webapp
systemctl stop asqatasun-server
```

## Launch Asqatasun manually

If you want more fine-grained control over startup, you can run directly the binaries.

### Start Asqatasun web application

```shell script
APP_PORT="8080"
DB_PORT="3306"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="/var/log/asqatasun/webapp"

/usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver     \
    -Dapp.logging.path="${APP_LOG_DIR}"           \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar /opt/asqatasun/asqatasun-webapp.war \
    --server.port="${APP_PORT}"
```

### Start Asqatasun server

```shell script
API_PORT="8081"
DB_PORT="3306"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
API_LOG_DIR="/var/log/asqatasun/api"

/usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver  \
    -Dapp.logging.path="${API_LOG_DIR}" \
    -Dapp.engine.loader.selenium.headless=true \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar /opt/asqatasun/asqatasun-rest-server.jar \
    --server.port="${API_PORT}" \
    --management.server.port=8091
```

### Stop Asqatasun server or Asqatasun web application

To stop Asqatasun, just kill the process.

## Use webapp

See [Asqatasun usage](../../../../User/Asqatasun-Usage/).

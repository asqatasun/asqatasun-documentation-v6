---
linkTitle: "Installation"
title: "Installation"
date: 2023-11-22T10:57:26+02:00
weight: 30
---
You should have already done these steps:

1. [Check Hardware provisioning](../Provisioning/)
2. [Check pre-requisites](../Pre-requisites/)

(Ever need help ? Go to
[Asqatasun Forum](https://forum.asqatasun.org).)

## Create user and directories

Asqatasun needs an actual system user to run. This is needed for Firefox (+ geckoDriver) to place its own directories
for runtime (like `~/.cache`, `~/.dconf` or `~/.mozilla`).

```shell script
ASQA_SYSTEM_USER="asqatasun"
ASQA_HOME="/home/${ASQA_SYSTEM_USER}"

useradd -c "Asqatasun user" -d "${ASQA_HOME}" -l -s /bin/bash "${ASQA_SYSTEM_USER}"
mkdir -p \
  /var/log/asqatasun/webapp/ \
  /var/log/asqatasun/server/ \
  /etc/asqatasun/ \
  "${ASQA_HOME}/.dconf"
chown -R asqatasun \
  /var/log/asqatasun/webapp/ \
  /var/log/asqatasun/server/ \
  /etc/asqatasun/ \
  "${ASQA_HOME}"
```

## Download Asqatasun binaries

Retrieve the lastest version of Asqatasun and extract
it on your file system.

```shell script
ASQA_VERSION="6.0.0-alpha.1"
cd /home/asqatasun
# Asqatasun server (API only)
curl \
  --output "./asqatasun-server-${ASQA_VERSION}.jar" \
  "https://gitlab.com/api/v4/projects/asqatasun%2FAsqatasun/packages/generic/Asqatasun/${ASQA_VERSION}/asqatasun-server-${ASQA_VERSION}.jar"
# Asqatasun Webapp (webapp only)
curl \
  --output "./asqatasun-web-app-${ASQA_VERSION}.war" \
  "https://gitlab.com/api/v4/projects/asqatasun%2FAsqatasun/packages/generic/Asqatasun/${ASQA_VERSION}/asqatasun-web-app-${ASQA_VERSION}.war"
```

Create symbolic links to binaries:

```shell script
cd "/home/asqatasun/"
WEBAPP_STUB="asqatasun-web-app"
API_STUB="asqatasun-server"
ln -s "${WEBAPP_STUB}-${ASQA_VERSION}.war" "${WEBAPP_STUB}.war"
ln -s "${API_STUB}-${ASQA_VERSION}.jar" "${API_STUB}.jar"
```

## Systemd unit: Asqatasun web application

Create configuration file. Please adjust credentials to your instance.

```shell script
touch /etc/asqatasun/asqatasun-webapp.yml
chown asqatasun /etc/asqatasun/asqatasun-webapp.yml
chmod 600 /etc/asqatasun/asqatasun-webapp.yml

unset HISTFILE    # Do not keep the following commands in history (credentials in plain text)
DB_DRIVER='mysql'
DB_HOST='localhost'
DB_PORT='3306'
DB_DATABASE='asqatasun'
DB_USER='asqatasunDatabaseUserLogin'
DB_PASSWORD='asqatasunDatabaseUserP4ssword'
WEBAPPURL='https://asqatasun.example.org'       # without trailing slash
ASQA_VERSION='6.0.0-alpha.1'

cat >/etc/asqatasun/asqatasun-webapp.yml <<EOF
---
jdbc:
  url: "jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}"
  user: "${DB_USER}"
  password: "${DB_PASSWORD}"
app:
  version: "${ASQA_VERSION}"
  emailSender:
    smtp:
      from: "admin@asqatasun.org"
  webapp:
    security:
      owasp:
        esapiPropertyValue: "/etc/asqatasun"
        tokenDurationValidity: 3600
    ui:
      config:
        authorizedPageSize: "10,25,50,100,250,1000,-1"
        authorizedRefForCriterionViewList: "Aw22,Rgaa30,Rgaa40"
        authorizedScopeForCriterionList: "DOMAIN,SCENARIO"
        authorizedScopeForPageList: "DOMAIN,SCENARIO"
        authorizedScopeForSynthesis: "DOMAIN,SCENARIO"
        authorizedScopeForTrend: "DOMAIN,SCENARIO"
        defaultReferential: "Rgaa40"
        forbiddenUserListForSettings: "guest"
        nbOfFailedPages: 10
        nbOfFailedTest: 5
        referentialWithModifiableTestWeight: "SEO,Aw22"
        userExclusionListForEmail: "guest"
        webAppUrl: "${WEBAPPURL}"
        contrastfinderServiceUrl: "https://app.contrast-finder.org"
        forgottenPassword:
          excludeUserList: "guest"
        orchestrator:
          ayncDelay: 30000
          emailSentToUserExclusionList: "guest"
          krashReportMailList: "support@asqatasun.org"
          enableKrashReport: true
EOF
```

Then create the systemd unit itself:

```shell script
cat >/etc/systemd/system/asqatasun-webapp.service <<EOF
[Unit]
Description=asqatasun-webapp
After=syslog.target

[Service]
User=asqatasun
ExecStart=/usr/bin/java                                                     \
    -Dspring.config.location=file:///etc/asqatasun/asqatasun-webapp.yml     \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox                            \
    -Dwebdriver.gecko.driver=/opt/geckodriver                               \
    -Dapp.logging.path=/var/log/asqatasun/webapp/                           \
    -jar asqatasun-web-app.war
SuccessExitStatus=143
WorkingDirectory=/home/asqatasun

[Install]
WantedBy=multi-user.target
EOF
```

Finally, enable it at boot time:

```shell
systemctl enable asqatasun-webapp.service
```

## Systemd unit: Asqatasun Server

Create configuration file. Please adjust credentials to your instance.

```shell script
touch /etc/asqatasun/asqatasun-server.yml
chown asqatasun /etc/asqatasun/asqatasun-server.yml
chmod 600 /etc/asqatasun/asqatasun-server.yml

unset HISTFILE    # Do not keep the following commands in history (credentials in plain text)
DB_DRIVER='mysql'
DB_HOST='localhost'
DB_PORT='3306'
DB_DATABASE='asqatasun'
DB_USER='asqatasunDatabaseUserLogin'
DB_PASSWORD='asqatasunDatabaseUserP4ssword'
ASQA_VERSION='6.0.0-alpha.1'

cat >/etc/asqatasun/asqatasun-server.yml <<EOF
---
jdbc:
  url: "jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}"
  user: "${DB_USER}"
  password: "${DB_PASSWORD}"
app:
  version: "${ASQA_VERSION}"
  engine:
    maxConcurrentSize:
      page: 5
EOF
```

Then create the systemd unit itself:

```shell script
cat >/etc/systemd/system/asqatasun-server.service <<EOF
[Unit]
Description=asqatasun-server
After=syslog.target

[Service]
User=asqatasun
ExecStart=/usr/bin/java                                                     \
    -Dspring.config.location=file:///etc/asqatasun/asqatasun-server.yml     \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox                            \
    -Dwebdriver.gecko.driver=/opt/geckodriver                               \
    -Dapp.logging.path=/var/log/asqatasun/server/                           \
    -jar asqatasun-server.jar
SuccessExitStatus=143
WorkingDirectory=/home/asqatasun

[Install]
WantedBy=multi-user.target
EOF
```

Finally, enable it at boot time:

```shell
systemctl enable asqatasun-server.service
```


## Next step

You can go to [Start Asqatasun](../Start-Asqatasun/)


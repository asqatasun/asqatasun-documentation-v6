---
linkTitle: "Provisioning"
title: "Hardware & network provisioning"
date: 2023-11-22T10:52:30+02:00
weight: 10
---

## Minimal hardware requirements

* RAM: 4 Gb
* CPU: dual-core (the more, the faster)
* Disk: 10Gb

## Notes on disk performance / usage

* For large (>10000 pages) and repetitive audits, an SSD hardware
  increases performances significantly.
* Statistically speaking, an audited page is about 0.3Mb on disk.

## Network flow

| Source host    | Destination host | Destination port | Detail                            |
|:---------------|:-----------------|:-----------------|:----------------------------------|
| Asqatasun host | the internet     | 80               | Site to be audited                |
| Asqatasun host | the internet     | 443              | Site to be audited                |
| Asqatasun host | Asqatasun host   | 8080             | Web application (embedded Tomcat) |
| Asqatasun host | Asqatasun host   | 8081             | API                               |
| Asqatasun host | Asqatasun host   | 8091             | Actuator (health check)           |
| Asqatasun host | Asqatasun host   | 3306             | Mysql / MariaDB (if chosen)       |
| Asqatasun host | Asqatasun host   | 5432             | Postgresql (if chosen)            |
| Asqatasun host | SMTP host        | 25 / 465         | SMTP                              |
| Asqatasun host | Asqatasun host   | 7055 to 7085     | Embedded Firefox                  |

## Next step

Go to [Pre-requisites](../Pre-requisites/).

---
title: "Docker"
date: 2020-07-22T19:06:46+02:00
weight: 10
---

See dedicated repository [Asqatasun-Docker](https://gitlab.com/asqatasun/asqatasun-docker)

For developers, Docker compositions can be
found [in source code in `docker/`](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/docker?ref_type=heads)

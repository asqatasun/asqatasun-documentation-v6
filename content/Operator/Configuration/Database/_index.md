---
linkTitle: "Database"
title: "Database"
date: 2024-05-29T09:23:43+01:00
---

## MariaDB or Postgresql

### `jdbc.url` for MariaDB

- JDBC connection string
- URL to connect to the database, compound with:
    1. The string `jdbc`
    2. The protocol, here `mariadb`
    3. The hostname and its port
    4. The name of the database
- Examples:
    - `jdbc:mariadb://localhost:3306/asqatasun`
    - `jdbc:mariadb://mariadb.example.org:3306/asqatasun`

### `jdbc.url` for Postgresql

- JDBC connection string
- URL to connect to the database, compound with:
    1. The string `jdbc`
    2. The protocol, here `postgresql`
    3. The hostname and its port
    4. The name of the database
- Examples:
    - `jdbc:postgresql://localhost:5432/asqatasun`
    - `jdbc:postgresql://pg.example.org:5432/asqatasun`

### `jdbc.user`

- String
- Username to connect to the Asqatasun database

### `jdbc.password`

- String
- Password to connect to the Asqatasun database

### `jdbc.url_query_options`

- String
- Options to pass to the database server
- Example: `?useSSL=false&useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8`

## HsqlDB

### `jdbc.url` for HsqlDB memory (recommended)

- JDBC connection string
- URL to connect to the database, compound with:
    1. The string `jdbc`
    2. The protocol, here `hsqldb`
    3. The "hostname" equivalent, here it is `mem` (for memory)
    4. The name of the database
- Example: `jdbc:hsqldb:mem:asqatasun`

### `jdbc.url` for HsqlDB file

- JDBC connection string
- URL to connect to the database, compound with:
    1. The string `jdbc`
    2. The protocol, here `hsqldb`
    3. The "hostname" equivalent, here it is `file`
    4. The "name" of the database, as a path
- Example: `jdbc:hsqldb:file:/tmp/asqatasundb`

### `jdbc.user` for HsqlDB

- String
- Username to connect to the Asqatasun database
- Mandatory value for HsqlDB: `SA` (for System Administrator)

### `jdbc.password` for HsqlDB

- String
- Password to connect to the Asqatasun database
- Mandatory value for HsqlDB: empty. (This must not be filled.)

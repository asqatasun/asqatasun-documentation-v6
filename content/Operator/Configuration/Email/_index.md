---
linkTitle: "Email"
title: "Configuration"
date: 2024-05-29T09:23:43+01:00
---

## `app.emailSender.smtp.debug`

- Boolean.
- Default value: `false`
- Enables (lots of) logs.

## `app.emailSender.smtp.from`

- Email.
- Default value: `asqatasun@example.com`

## `app.emailSender.smtp.host`

- Hostname.
- Default value: `localhost`
- Examples: `smtp01.mycompany.com`, `in1.mailjet.com`, `smtp.sendinblue.com`

## `app.emailSender.smtp.port`

- Relative.
- Default value: `-1`
- Example: `465`

## `app.emailSender.smtp.user`

- String.
- Default value: empty
- Example: `service.asqatasun.prod@mycompany.com`

## `app.emailSender.smtp.password`

- String.
- Default value: empty
- Example: `A-Re@lly-LONG-and-a-b1t-complex!-phrase`

## `app.emailSender.smtp.starttls.enable`

- Boolean.
- Default value: `false`
- `starttls` and `ssl` are mutually exclusive.

## `app.emailSender.smtp.ssl.enable`

- Boolean.
- Default value: `false`
- `starttls` and `ssl` are mutually exclusive.

## `app.emailSender.smtp.ssl.protocols`

- String.
- Default value: `TLSv1.2`

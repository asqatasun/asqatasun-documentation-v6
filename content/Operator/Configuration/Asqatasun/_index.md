---
linkTitle: "Application"
title: "Asqatasun configuration"
date: 2024-05-29T09:23:43+01:00
---

## URL of the Asqatasun instance `app.webapp.ui.config.webAppUrl`

- URL
- Default value: `http://localhost:8080/`
- Complete URL to connect to web-application. This option is needed to send info and links by email ; for instance
  when a long audit is terminated, or when recovering a lost password.
- Examples:
    * `https://accessibility.example.org/asqatasun/`
    * `https://asqatasun.example.org/`
    * `http://localhost:8080/`

## Add analytics (Matomo)

### `app.webapp.ui.matomoUrl`

- URL
- Default value: empty
- Example: `https://matomo.mycompany.com`

### `app.webapp.ui.matomoSiteId`

- Integer
- Default value: empty
- The id given by Matomo for monitoring the Asqatasun instance
- Example: `11`

## Custom additional logo

Add your own logo next to Asqatasun logo.

### `app.webapp.ui.bannerCustomImage`

- Base64 encoded image
- Default value: empty
- Example: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAk...`

### `app.webapp.ui.bannerCustomImageAlt`

- Text
- Default Value: empty
- The alternative text for the logo

### `app.webapp.ui.bannerCustomImageCss`

- Inline CSS
- Default Value: empty
- Custom inline CSS to adjust the logo
- Example: `margin-top:3px; margin-left:3px;`

## Crash report

### `app.webapp.ui.config.orchestrator.enableKrashReport`

- Boolean
- Default value: `true`
- If the application ever crashes, a message with the stack trace can be sent by email. The recipient of the message
  is defined by `app.webapp.ui.config.krashReportMailList`.

### `app.webapp.ui.config.orchestrator.krashReportMailList`

- Email
- Default value: `support@asqatasun.org`
- List of emails (comma-separated list) used to send crash reports. If `enableKrashReport` property is set
  to false, this option is ignored, but has to be present, even empty.

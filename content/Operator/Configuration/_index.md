---
linkTitle: "Configuration"
title: "Configuration"
date: 2023-12-22T09:23:43+01:00
weight: 10
---

Configuration is made through files `asqatasun-webapp.yaml` and `asqatasun-server.yaml`.

Values can be overridden
following [Springboot externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config)

# Documentation

Asqatasun v6.x Documentation. Content of <https://doc.asqatasun.org/v6/>

## Pre-requisites

* [Install Hugo](https://gohugo.io/getting-started/installing/), cross-platform binary is recommended
  (package from Ubuntu is not, for being too old).
* This repository uses submodules, [see clone instructions](https://doc.asqatasun.org/v5/en/Developer/Documentation/)
  to avoid errors related to uncloned submodules.

## Compilation

For local development:

```shell script
hugo serve --i18n-warnings --debug -v
```

To generate static pages:

```shell script
hugo
```

All static pages are placed in `public/` directory
